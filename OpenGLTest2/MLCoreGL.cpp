#include "MLCoreGL.h"

using namespace MLRenderer;

Mat4f	MLCoreGL::m_ortho;

MLCoreGL::MLCoreGL(void)
{
	m_hDC = NULL;
	m_hRC = NULL;
}


MLCoreGL::~MLCoreGL(void)
{
}


bool MLCoreGL::Bind(HWND hWnd, GLsizei Width, GLsizei Height, int BitCount)
{
	GLuint		PixelFormat;						//Store details for pixel format

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		BitCount,									// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		16,											// 16Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

	m_hDC = GetDC(hWnd);							//Get device context

	if (!m_hDC)
	{
		MessageBox(hWnd, "Could not set device context", "GL ERROR", MB_ICONERROR);
		return false;
	}

	PixelFormat=ChoosePixelFormat(m_hDC,&pfd);		//Let windows choose pixel format

	SetPixelFormat(m_hDC,PixelFormat,&pfd);			//Set format chosen

	m_hRC=wglCreateContext(m_hDC);						//Get rendering context

	if (!m_hRC)
	{
		MessageBox(hWnd, "Could not set render context", "GL ERROR", MB_ICONERROR);
		return false;
	}

	wglMakeCurrent(m_hDC,m_hRC);						//Activate rendering context	


	//initalises glew to enable the linkage to 4.0
	GLenum err = glewInit();
	const GLubyte* GLEWError;
	if( GLEW_OK != err )
	{
		MessageBox(hWnd, "Could not Initalise GLEW to enable 4.0", "GL ERROR", MB_ICONERROR);
		GLEWError = glewGetErrorString(err);
	}

	const GLubyte *renderer = glGetString( GL_RENDERER );
	const GLubyte *vendor = glGetString( GL_VENDOR );
	const GLubyte *version = glGetString( GL_VERSION );
	const GLubyte *glslVersion = glGetString( GL_SHADING_LANGUAGE_VERSION );
	MLError::ErrorHandler::printError((char*)renderer);
	MLError::ErrorHandler::printError((char*)vendor);
	MLError::ErrorHandler::printError((char*)version);
	MLError::ErrorHandler::printError((char*)glslVersion);

	GLint major, minor;
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);

	SetForegroundWindow(hWnd);			// Slightly Higher Priority
	SetFocus(hWnd);						// Sets Keyboard Focus To The Window

	glViewport(0,0,Width-8,Height-30);		//set viewport to size of window

	//m_ortho = glm::ortho(0.0f, (float)Width, (float)Height, 0.0f, 10.0f, -10.0f);
	MLMath::MLMat4::ortho(m_ortho, 0.0f, (float)Height,  0.0f, (float)Width, 10.0f, -10.0f);

	MLError::ErrorHandler::printMat(m_ortho);

	return true;
}

void MLCoreGL::setOrtho(float Left, float Right, float Bottom, float Top, float zNear, float zFar)
{
	//m_ortho = glm::ortho(Left, Right, Bottom, Top, zNear, zFar);
	MLMath::MLMat4::ortho(m_ortho, Top, Bottom, Left, Right, zNear, zFar);
}

void MLCoreGL::initaliseCore()
{
	glClearColor(0.26f, 0.66f, 0.78f, 1.0f);				// Black Background
	//glEnable (GL_BLEND);
	//glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glClearDepth(1.0f);									// Depth Buffer Setup
 	//glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
 	//glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
}

void MLCoreGL::startFrame()
{
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
}

void MLCoreGL::endFrame()
{
	SwapBuffers(m_hDC);
}



bool MLRenderer::ResizeGL(GLsizei Width, GLsizei Height)
{
	glViewport(0,0,Width,Height);		//set viewport to size of window
	//glCore::setOrtho(0.0f, (float)Width, (float)Height, 0.0f, 10.0f, -10.0f);
	return true;
}
