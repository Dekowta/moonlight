#include <windows.h>
#include <gl/glew.h>
#include <gl/GL.h>		
#include <gl/GLU.h>
#include "ErrorHandler.h"
// #include <glm/glm.hpp>
// #include <glm/gtc/matrix_transform.hpp>
#include <MLMath/MLMath.h>
#pragma once

using namespace MLMath::MLMat4;

namespace MLRenderer 
{
	bool ResizeGL(GLsizei Width, GLsizei Height);				//resize the openGL window

	class MLCoreGL
	{
	public:
		MLCoreGL(void);
		virtual ~MLCoreGL(void);

		void initaliseCore();
		void startFrame();
		void endFrame();

		Mat4f static getOrtho() { return m_ortho; }
		void static setOrtho(float Left, float Right, float Bottom, float Top, float zNear, float zFar);

		bool Bind(HWND hWnd, int Width, int Height, int BitCount);
	private:
		HDC			m_hDC;		// Private GDI Device Context
		HGLRC		m_hRC;		// Permanent Rendering Context
		static Mat4f		m_ortho;    //stores the orthographic projection
	};
}



