#include "MLCamera.h"
#include "MLShader.h"
#include "MLCoreGL.h"
#include "MLTextureManager.h"
#include <FreeImage.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#pragma once

using glm::vec2;
using glm::mat4;


class basicObject
{
public:
	basicObject(void);
	virtual ~basicObject(void);

	void createOBJ();

	void setPosition(vec2 pos);
	void setPosition(float x, float y);
	
	void renderObj();
private:
	void LoadTexture(const char* filelocation);

private:
	MLRenderer::MLCamera* View;
	GLuint vaoHandle;
	mat4 ObjMat;
	MLRenderer::MLShader* Shader;
};

