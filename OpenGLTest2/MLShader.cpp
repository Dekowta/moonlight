#include "MLShader.h"

using namespace MLRenderer;

MLShader::MLShader(void)
{
	m_program = 0;
	m_shaders[MLS_VERT]=0;
	m_shaders[MLS_FRAG]=0;
	m_shaders[MLS_GEOM]=0;
}


MLShader::~MLShader(void)
{
}


bool MLShader::LoadShaderFile(const char* File, GLenum type)
{	
	std::ifstream inStream;
	inStream.open(File, std::ios_base::in | std::ios::binary);
	if(inStream) 
	{
		int fileSize = 0;
		inStream.seekg(0,std::ios::end);
		fileSize = (int)inStream.tellg();
		inStream.seekg(0,std::ios::beg);
		if (fileSize == -1)
		{
			MLError::ErrorHandler::printError(__TIME__": Error getting shader file size");
			return false;
		}

		char* sSource = (char*)malloc((fileSize/sizeof(char)) + 1);

		inStream.read(sSource, fileSize);
		sSource[fileSize] = '\0';

		return compileShader(sSource, type);

	}
	else
	{
		MLError::ErrorHandler::printError(__TIME__": Could not load file");
		return false;
	}
	
	return true;
}

bool MLShader::LoadShaderString(const char* Source, GLenum type)
{
	bool result;
	result = compileShader(Source, type);
	return result;
}


bool MLShader::compileShader(const char* sSource, GLenum type)
{
	GLuint shader = glCreateShader(type);

	if (!shader)
	{
		MLError::ErrorHandler::printError(__TIME__": Error Could not create Shader");
		return false;
	}

	glShaderSource(shader, 1, &sSource, NULL);
	glCompileShader(shader);
	GLint result;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		MLError::ErrorHandler::printError(__TIME__": Error Could not compile Shader");
		GLint LogLen;
		glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &LogLen );
		if( LogLen > 0 )
		{
			char* log = (char* )malloc(LogLen);
			GLsizei written;
			glGetShaderInfoLog(shader, LogLen, &written, log);
			MLError::ErrorHandler::printError(__TIME__": Shader Log");
			MLError::ErrorHandler::printError(log);
			free(log);
			return false;
		}
		return false;
	}

	switch(type)
	{
		case GL_FRAGMENT_SHADER:
			m_shaders[MLS_FRAG] = shader;
			break;
		case GL_VERTEX_SHADER:
			m_shaders[MLS_VERT] = shader;
			break;
		case GL_GEOMETRY_SHADER:
			m_shaders[MLS_GEOM] = shader;
			break;
	}

	return true;
}


bool MLShader::LinkShaders()
{
	char* error = new char[50];

	if (m_shaders[MLS_FRAG] != 0)
	{
		glAttachShader (m_program, m_shaders[MLS_FRAG]);
	}
	if (m_shaders[MLS_VERT] != 0)
	{
		glAttachShader (m_program, m_shaders[MLS_VERT]);
	}
	if (m_shaders[MLS_GEOM] != 0)
	{
		glAttachShader (m_program, m_shaders[MLS_GEOM]);
	}

	glLinkProgram(m_program);
	
	
	GLint status;
	glGetProgramiv( m_program, GL_LINK_STATUS, &status );
	if( GL_FALSE == status ) {
		MLError::ErrorHandler::printError(__TIME__": Failed to link shader");
		GLint logLen;
		glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &logLen);
		if( logLen > 0 )
		{
			char * log = (char *)malloc(logLen);
			GLsizei written;
			glGetProgramInfoLog(m_program, logLen, &written, log);
			MLError::ErrorHandler::printError(__TIME__": Program Log");
			MLError::ErrorHandler::printError(log);
			free(log);
			return false;
		}
		return false;
	}

	//printActiveAtts();
	//printActiveUniform();

	return true;
}


void MLShader::bindAtt(attID type, char* Name)
{
	glBindAttribLocation(m_program, (GLuint)type, Name);
	m_attributes[Name] = type;
};

void MLShader::addAtt(char* Name)
{
	m_attributes[Name] = glGetAttribLocation(m_program, Name);
}

void MLShader::addUniform(char* Name)
{
	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
}

void MLShader::addUniform(char* Name, bool inVal)
{
	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
	glUniform1i(m_uniforms[Name], inVal);
}

void MLShader::addUniform(char* Name, int inVal)
{
	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
	glUniform1i(m_uniforms[Name], inVal);
}

void MLShader::addUniform(char* Name, float inVal)
{
	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
	glUniform1f(m_uniforms[Name], inVal);
}

void MLShader::addUniform(char* Name, const Vector2f &inVec)
{
	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
	glUniform2fv(m_uniforms[Name],1, (GLfloat*)toptr(inVec));
}

void MLShader::addUniform(char* Name, const Vector3f &inVec)
{
	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
	glUniform3fv(m_uniforms[Name],1, (GLfloat*)toptr(inVec));
}

void MLShader::addUniform(char* Name, const Vector4f &inVec)
{
	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
	glUniform4fv(m_uniforms[Name],1, (GLfloat*)toptr(inVec));
}
// 
// void MLShader::addUniform(char* Name, const mat2 &inMat)
// {
// 	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
// 	glUniformMatrix2fv(m_uniforms[Name],1, GL_FALSE, &inMat[0][0]);
// }
// 
// void MLShader::addUniform(char* Name, const mat3 &inMat)
// {
// 	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
// 	glUniformMatrix3fv(m_uniforms[Name],1, GL_FALSE, &inMat[0][0]);
// }

void MLShader::addUniform(char* Name, const Mat4f &inMat)
{
	m_uniforms[Name] = glGetUniformLocation(m_program, Name);
	glUniformMatrix4fv(m_uniforms[Name],1, GL_FALSE, (GLfloat*)toptr(inMat));
}

void MLShader::lock(bool bind)
{
	if (m_program == 0)
	{
		m_program = glCreateProgram();
		return;
	}
//	if (bind == false)
//	{
	glUseProgram(m_program);
//	}
}

void MLShader::printActiveAtts()
{
	GLint maxLength, nAttribs;
	glGetProgramiv(m_program, GL_ACTIVE_ATTRIBUTES, &nAttribs);				//get the count of attributes
	glGetProgramiv(m_program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxLength);	//get the string length of the largest active attribute

	GLchar * name = (GLchar *) malloc( maxLength ); //create a string to hold the largest attribute

	GLint written, size, location;
	GLenum type;
	MLError::ErrorHandler::printError("ACTIVE ATTRIBUTES");
	MLError::ErrorHandler::printError("------------------------------------------------");
	for( int i = 0; i < nAttribs; i++ ) 
	{
		glGetActiveAttrib( m_program, i, maxLength, &written, &size, &type, name );
		location = glGetAttribLocation(m_program, name);
		MLError::ErrorHandler::printError("NAME: ");
		MLError::ErrorHandler::printError(name);
		sprintf(name, "%d", location);
		MLError::ErrorHandler::printError("INDEX: ");
		MLError::ErrorHandler::printError(name);
		MLError::ErrorHandler::printError("------------------------------------------------");
	}
	free(name);

}

void MLShader::printActiveUniform()
{
	GLint maxLength, nAttribs;
	glGetProgramiv(m_program, GL_ACTIVE_UNIFORMS, &nAttribs);				//get the count of attributes
	glGetProgramiv(m_program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxLength);	//get the string length of the largest active attribute

	GLchar * name = (GLchar *) malloc( maxLength ); //create a string to hold the largest attribute

	GLint written, size, location;
	GLenum type;
	MLError::ErrorHandler::printError("ACTIVE UNIFORMS");
	MLError::ErrorHandler::printError("------------------------------------------------");
	for( int i = 0; i < nAttribs; i++ ) 
	{
		glGetActiveUniform( m_program, i, maxLength, &written, &size, &type, name );
		location = glGetUniformLocation(m_program, name);
		MLError::ErrorHandler::printError("NAME: ");
		MLError::ErrorHandler::printError(name);
		sprintf(name, "%d", location);
		MLError::ErrorHandler::printError("INDEX: ");
		MLError::ErrorHandler::printError(name);
		MLError::ErrorHandler::printError("------------------------------------------------");
	}
	free(name);

}