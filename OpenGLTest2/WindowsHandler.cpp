#include "WindowsHandler.h"


//static prototyping
HWND	WindowsHandler::_hWnd;
int		WindowsHandler::_wHeight;
int		WindowsHandler::_wWidth;
bool	WindowsHandler::_FullScreen;
int		WindowsHandler::_BitMode;


LRESULT CALLBACK WndProcedure(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uMsg)
	{
		// If the user wants to close the application
	case WM_DESTROY:
		{
			// then close it
			MLError::ErrorHandler::endFile();
			PostQuitMessage(WM_QUIT);
			break;	
		}
	case WM_SIZE:
		{
			MLRenderer::ResizeGL(LOWORD(lParam), HIWORD(lParam));
			break;
		}
	default:
		// Process the left-over messages
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	// If something was not done, let it go
	return 0;	
}



WindowsHandler::WindowsHandler(void)
{
}

WindowsHandler::WindowsHandler(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow) : m_hInstance(hInstance), m_hPrevInstance(hPrevInstance),
	m_lpCmdLine(lpCmdLine), m_nCmdShow(nCmdShow), m_clsName("MoonLight")
{

}

WindowsHandler::~WindowsHandler(void)
{
}

bool WindowsHandler::release()
{
	if (_hWnd && !DestroyWindow(_hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		_hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass(m_clsName ,m_hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		m_hInstance=NULL;									// Set hInstance To NULL
	}

	return true;
}

bool WindowsHandler::createWindow(LPCTSTR title, int width, int height, int BitCount, bool Console)
{
	if (Console == true)
		loadConsole();

	std::cout << WINHAND"Creating Window" << std::endl;

	_wWidth		= width;
	_wHeight	= height;
	_BitMode	= BitCount;

	if (MessageBox(NULL,"Would You Like To Run In Fullscreen Mode?", "Start FullScreen?",MB_YESNO|MB_ICONQUESTION)==IDNO)
		_FullScreen = false;
	else
		_FullScreen = true;
	WNDCLASS WndCls;

	// Create the application window
	m_hInstance			 = GetModuleHandle(NULL);
	WndCls.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndCls.lpfnWndProc   = (WNDPROC)WndProcedure;
	WndCls.cbClsExtra    = 0;
	WndCls.cbWndExtra    = 0;
	WndCls.hInstance	 = m_hInstance;
	WndCls.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
	WndCls.hCursor       = LoadCursor(NULL, IDC_ARROW);
	WndCls.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndCls.lpszMenuName  = NULL;
	WndCls.lpszClassName = m_clsName;
	WndCls.hInstance     = m_hInstance;

	// Register the application
	if (!RegisterClass(&WndCls))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","WIN_ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;											// Return FALSE
	}

	if (_FullScreen)
	{
		DEVMODE dmScreenSettings;								// Device Mode
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);		// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth	= _wWidth;				// Selected Screen Width
		dmScreenSettings.dmPelsHeight	= _wHeight;				// Selected Screen Height
		dmScreenSettings.dmBitsPerPel	= _BitMode;				// Selected Bits Per Pixel
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Use Windowed Mode.
			if (MessageBox(NULL,"The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?","WIN_ERROR",MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
			{
				_FullScreen=FALSE;		// Windowed Mode Selected.  Fullscreen = FALSE
			}
			else
			{
				// Pop Up A Message Box Letting User Know The Program Is Closing.
				MessageBox(NULL,"Program Will Now Close.","WIN_ERROR",MB_OK|MB_ICONSTOP);
				return FALSE;									// Return FALSE
			}
		}
	}

	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style

	if (_FullScreen)											// Are We Still In Fullscreen Mode?
	{
		dwExStyle=WS_EX_APPWINDOW;								// Window Extended Style
		dwStyle=WS_POPUP;										// Windows Style
		ShowCursor(FALSE);										// Hide Mouse Pointer
	}
	else
	{
		dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
		dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	}



	// Create the window object
	if(!(_hWnd = CreateWindowEx(dwExStyle,
		m_clsName,
		title,
		WS_CLIPSIBLINGS |					// Required Window Style
		WS_CLIPCHILDREN |					// Required Window Style
		dwStyle ,							// Defined Window Style,				
		CW_USEDEFAULT, CW_USEDEFAULT,								// Window Position
		_wWidth,	// Calculate Window Width
		_wHeight,	// Calculate Window Height
		NULL,								// No Parent Window
		NULL,								// No Menu
		m_hInstance,							// Instance
		NULL)))
	{
		MessageBox(NULL,"Window Creation Error.","WIN_ERROR",MB_OK|MB_ICONEXCLAMATION);
		return FALSE;								// Return FALSE
	}
	
	// Display the window to the user
	ShowWindow(_hWnd, SW_SHOWNORMAL);
	UpdateWindow(_hWnd);

	std::cout << WINHAND"Window Created" << std::endl;

	return true;
}


void WindowsHandler::loadConsole()
{
	int hConHandle;

	long lStdHandle;

	CONSOLE_SCREEN_BUFFER_INFO coninfo;

	FILE *fp;

	// allocate a console for this app

	AllocConsole();

	// set the screen buffer to be big enough to let us scroll text

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),&coninfo);

	coninfo.dwSize.Y = MAX_CONSOLE_LINES;

	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),11);

	// redirect unbuffered STDOUT to the console

	lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "w" );

	*stdout = *fp;

	setvbuf( stdout, NULL, _IONBF, 0 );

	// redirect unbuffered STDIN to the console

	lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);

	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "r" );

	*stdin = *fp;

	setvbuf( stdin, NULL, _IONBF, 0 );

	// redirect unbuffered STDERR to the console

	lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);

	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen( hConHandle, "w" );

	*stderr = *fp;

	setvbuf( stderr, NULL, _IONBF, 0 );

	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog

	// point to console as well

	std::ios_base::sync_with_stdio();
}



bool WindowsHandler::setTimer(UINT Amount, LPTIMECALLBACK TimeProgram)
{
	TIMECAPS tc;
	timeGetDevCaps(&tc, sizeof(TIMECAPS));
	char* MSF = new char[50];
	sprintf(MSF, "MIN SCOPE: %d", tc.wPeriodMin);
	MLError::ErrorHandler::printError(MSF);
	sprintf(MSF, "MIN SCOPE: %d", tc.wPeriodMax);
	MLError::ErrorHandler::printError(MSF);
	free(MSF);
	timeBeginPeriod(tc.wPeriodMin);

	timeSetEvent(Amount, tc.wPeriodMax, TimeProgram, NULL, TIME_PERIODIC);

// 	UINT result = SetTimer(_hWnd, Name, Amount, TimeProgram);
// 	if (result == 0)
// 		return false;

	return true;
}

bool WindowsHandler::update()
{
	if(PeekMessage(&m_msg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&m_msg);
		DispatchMessage(&m_msg);

		if(m_msg.message == WM_QUIT)
			return 0;
	}

	//keyboard input
	if(m_msg.wParam == VK_RETURN)
	{
		//do some stuff on keypress
		return false;
	}

	return true;
}