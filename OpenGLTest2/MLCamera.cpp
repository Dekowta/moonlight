#include "MLCamera.h"

using namespace MLRenderer;

MLCamera::MLCamera(void)
{
}


MLCamera::~MLCamera(void)
{
}


void MLCamera::create()
{
	m_view = glm::mat4(1.0f);
}

void MLCamera::create(float x, float y)
{
	 m_view = glm::mat4(1.0f);
	 //m_view - glm::lookAt(vec3(x, y, 1.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));
	 m_view = glm::translate(m_view, vec3(x, y, 0.0f));
}

void MLCamera::create(float x, float y, float rot)
{
	m_view = glm::mat4(1.0f);
	m_view = glm::translate(m_view, vec3(x, y, 0.0f));
	m_view = glm::rotate(m_view, rot, vec3(0.0f, 0.0f, 1.0f));
}


void MLCamera::setCamPos(float x, float y)
{
	m_view = glm::translate(m_view, vec3(x, y, 0.0f));
}

void MLCamera::setCamRot(float rot)
{
	m_view = glm::rotate(m_view, rot, vec3(0.0f, 0.0f, 1.0f));
}