#include <iostream>
#include <fstream>
#include <cstdio>
#include <windows.h>
#include <gl/GL.h>		
#include <gl/GLU.h>
#include <iostream>
#include <MLMath/MLMath.h>
#pragma once

namespace MLError
{
	class ErrorHandler
	{
	public:
		ErrorHandler(void);
		virtual ~ErrorHandler(void);
		void setErrorLoc(const char* Location) { fileLoc = Location; }
		static void printError(const char* Error);
		static void printGLError(const char* inError);
		static void printMat(MLMath::MLMat4::Mat4f Matrix4x4);
		static void endFile();
	private:
		static void createFile();
		static std::ofstream ErrorFile;
		static const char* fileLoc;
		static bool fileOpen;
	};

}

