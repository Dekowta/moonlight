#include "WindowsHandler.h"
#include "MLCoreGL.h"
#include "MLShader.h"
#include "MLSprite.h"
#include "MLSpriteBatch.h"
#include "general.h"
#include "ErrorHandler.h"
#include "basicObject.h"
//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
#include <MLMath/MLMath.h>
#pragma once

using namespace MLMath::MLMat4;
using namespace MLMath::MLVector4;
using namespace MLMath::MLVector3;
using namespace MLMath::MLVector2;


namespace MLApp
{
	extern int frameCount;

	class ApplicationMain
	{
	public:
		ApplicationMain(void);
		ApplicationMain(WindowsHandler* wHnd);
		virtual ~ApplicationMain(void);
	
		bool initaliseApp(); //intialises the application
		bool updateApp();	 //updates the application
		bool BindGLWindow();
		bool release();

	private:
		MLRenderer::MLCoreGL*			m_cGL;
		WindowsHandler*					m_wHnd;
		MLError::ErrorHandler*			m_error;



	//TEMP VARIABLES
	////////////////////////////////////////////////////
	private:
		basicObject* OBJ1;
		basicObject* OBJ2;
		MLRenderer::MLSprite* sprite1;
		MLRenderer::MLSprite* sprite2;
		MLRenderer::MLSpriteBatch* Batch;
		Vector2f angle;

		char* error;

	};
}

