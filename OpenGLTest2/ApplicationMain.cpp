////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	ApplicationMain.cpp
//
// summary:	This is the Core and central hub for all the framework in the engine
//			This will only touch upon the window to initalise it with openGL but apart from that#
//			there should only be rendering or managment code in here 
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "ApplicationMain.h"


using namespace MLApp;

ApplicationMain::ApplicationMain(void)
{
}

ApplicationMain::ApplicationMain(WindowsHandler* wHnd) : m_wHnd(wHnd)
{
}

ApplicationMain::~ApplicationMain(void)
{
}


bool ApplicationMain::initaliseApp()
{
	angle = Vector2f(1.0f, 1.0f);

	error = new char[50];
	m_error->setErrorLoc("Log.txt");
	//This is now the core applcation and should not interface much with the window apart from when binding OpenGL to the window
	m_cGL	 = new MLRenderer::MLCoreGL();
	BindGLWindow();

	m_cGL->initaliseCore();
	sprite1 = new MLRenderer::MLSprite();
	sprite1->setPos(0, 0);
	sprite1->setSize(230, 230);
	sprite1->setSource(Vector4f(0, 0, 200, 200));
	sprite1->loadTexture("textures/Test2.png");

	sprite2 = new MLRenderer::MLSprite();
	sprite2->setPos(50, 50);
	sprite2->setSize(230, 230);
	sprite2->setSource(Vector4f(0, 0, 200, 200));
	sprite2->loadTexture("textures/Test3.jpg");

	MLError::ErrorHandler::printGLError("BEFORE BATCH CREATION");
	Batch = new MLRenderer::MLSpriteBatch();

	//OBJ1 = new basicObject();
	//OBJ2 = new basicObject();
	//OBJ1->createOBJ();
	//MLError::ErrorHandler::printGLError("BEFORE BIND OBJ2");
	//OBJ2->createOBJ();

	//OBJ2->setPosition(400, 300);
	return true;
}

bool ApplicationMain::updateApp()
{
	//if( angle >= 10.0f) angle -= 10.0f;

	

	m_cGL->startFrame();
	//OBJ1->renderObj();
	//OBJ2->renderObj();
	Batch->Begin();
	for(int i = 0; i < 5000; i++)
	{
		Batch->Draw(sprite2);
		//Batch->Draw(sprite2->getTexture(), vec2(0.0f), vec2(230.0f));
	}

	//for(int i = 0; i < 1; i++)
	//	Batch->Draw(sprite1);

	Batch->End();
	m_cGL->endFrame();

// 	float result = (float)MLApp::frameCount/1000.0f;
// 	char* MSF = new char[50];
// 	sprintf(MSF, "MS/F: %f", result);
// 	WindowsHandler::setWindowText(MSF);
// 	free(MSF);
	MLApp::frameCount++;

	return true;
}

bool ApplicationMain::release()
{
	delete m_cGL;
	return true;
}

bool ApplicationMain::BindGLWindow()
{
	return m_cGL->Bind(WindowsHandler::_hWnd, WindowsHandler::_wWidth, WindowsHandler::_wHeight, WindowsHandler::_BitMode);
}

