#include "ErrorHandler.h"

using namespace MLError;

std::ofstream ErrorHandler::ErrorFile;
const char* ErrorHandler::fileLoc;
bool ErrorHandler::fileOpen;

ErrorHandler::ErrorHandler(void)
{
}


ErrorHandler::~ErrorHandler(void)
{
}

void ErrorHandler::createFile()
{
	ErrorFile.open(fileLoc);
	fileOpen = true;
}

void ErrorHandler::printError(const char* Error)
{
	std::cout << Error << std::endl;
}

 void ErrorHandler::printMat(MLMath::MLMat4::Mat4f Matrix4x4)
{
	printf("PRINTING MATRIX\n");
	printf("00 = %4.3f 10= %4.3f 20= %4.3f 30=%4.3f\n", Matrix4x4.m[0][0], Matrix4x4.m[1][0], Matrix4x4.m[2][0], Matrix4x4.m[3][0]);
	printf("01 = %4.3f 12= %4.3f 21= %4.3f 31=%4.3f\n", Matrix4x4.m[0][1], Matrix4x4.m[1][1], Matrix4x4.m[2][1], Matrix4x4.m[3][1]);
	printf("02 = %4.3f 13= %4.3f 22= %4.3f 32=%4.3f\n", Matrix4x4.m[0][2], Matrix4x4.m[1][2], Matrix4x4.m[2][2], Matrix4x4.m[3][2]);
	printf("03 = %4.3f 14= %4.3f 23= %4.3f 33=%4.3f\n", Matrix4x4.m[0][3], Matrix4x4.m[1][3], Matrix4x4.m[2][3], Matrix4x4.m[3][3]);
}

void ErrorHandler::endFile()
{
	ErrorFile.close();
}


void ErrorHandler::printGLError(const char* inError)
{
	char* error = new char[50];
	printError(inError);
	sprintf(error, "GL Error: %d", glGetError());
	printError(error);
	free(error);

}