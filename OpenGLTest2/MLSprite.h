#include "MLCoreGL.h"
#include "MLTextureManager.h"
//#include <glm\glm.hpp>
#include <MLMath\MLMath.h>
#pragma once

using namespace MLMath::MLVector2;
using namespace MLMath::MLVector4;

namespace MLRenderer
{
	class MLSprite
	{
	public:
		MLSprite(void) {};
		virtual ~MLSprite(void) {};

		void loadTexture(const char* path);
		
		void enable()	{m_enabled=true;}
		void disable()	{m_enabled=false;}

		/*setters*/
		//Position
		void setPos(float x, float y)		{m_PosX=x; m_PosY=y;}
		void setPos(Vector2f pos)			{m_PosX=pos.x; m_PosY=pos.y;}
		void setPosX(float x)				{m_PosX=x;}
		void setPosY(float y)				{m_PosY=y;}
		//size (Width/Height)
		void setSize(float width, float height) {m_width=width; m_height=height;}
		void setSize(Vector2f size)				{m_width=size.x; m_height=size.y;}
		void setWidth(float width)				{m_width=width;}
		void setHeight(float height)			{m_height=height;}
		//source (X/Y/Width/Height)
		void setSource(float x, float y, float width, float height)		{m_sourceX=x; m_sourceY=y; m_sourceWidth=width; m_sourceHeight=height;}
		void setSource(Vector4f source)									{m_sourceX=source.x; m_sourceY=source.y; m_sourceWidth=source.z; m_sourceHeight=source.w;}


		/*getters*/
		Vector2f getPos()			{return Vector2f(m_PosX, m_PosY);}
		//float getPosX
		//float getPosY
		Vector2f getSize()			{return Vector2f(m_width, m_height);}
		//float getWidth
		//float getHeight
		Vector4f getSource()		{return Vector4f(m_sourceX, m_sourceY, m_sourceWidth, m_sourceHeight);}

		GLuint getTexture()			{return m_texture;}

		bool getEnabled()			{return m_enabled;}

	private:
	
		/* Width and height of the sprite */
		float m_PosX;
		float m_PosY;
		float m_width;
		float m_height;

		/* Will likely not be used for a while */
		float m_OriginX;
		float m_OriginY;

		/* Texture source rectangle */ 
		float m_sourceX;
		float m_sourceY;
		float m_sourceWidth;
		float m_sourceHeight;

		/* Sprite is enabled or not */
		bool m_enabled;

		/* Texture ID */
		GLuint m_texture;
	};

	inline void MLSprite::loadTexture(const char* path)
	{
		m_texture = MLRenderer::MLTextureManager::loadTexture(path);
	}
}