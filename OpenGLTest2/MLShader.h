////////////////////////////////////////////////////////////////////////////////////////////////////
// file:	ShaderML.h
//
// summary:	This is a class that can be used to store a fragment, vertex and geometry shader
//			and set attributes for each one.
//			
//			I have mimicked Movania Muhammad Mobeen's GLSL shader class which can be found:
//			http://mmmovania.blogspot.co.uk/2011/02/c-class-for-glslshader.html
//			I have made some slight modifications and improvements to the class from Movania's
//			version
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "MLCoreGL.h"
#include <unordered_map>
// #include <glm/glm.hpp>
// #include <glm/gtc/type_ptr.hpp>
#include <MLMath/MLMath.h>
#pragma once

// using glm::vec2;
// using glm::vec3;
// using glm::vec4;
// using glm::mat2;
// using glm::mat3;
// using glm::mat4;

using namespace MLMath::MLVector2;
using namespace MLMath::MLVector3;
using namespace MLMath::MLVector4;
using namespace MLMath::MLMat4;

namespace MLRenderer
{
	//This will be used to list all the attributes that can be passed
	//to the shader for any type
	typedef enum attID
	{
		ML_ATT_VERTEXPOS = 0,
		ML_ATT_VERTEXCOL = 1,
		ML_ATT_TEXTURE0 = 2
	}attID;


	class MLShader
	{
	public:
		MLShader(void);
		virtual ~MLShader(void);

		bool LoadShaderFile(const char* File, GLenum type);		//used to load a shader form a file
		bool LoadShaderString(const char* Source, GLenum type);	//used to load a shader from a string
		bool LinkShaders();										//will link the shaders to the program

		void lock(bool bind = false);							//will lock the current program 
		void unlock()	{ glUseProgram(0); }					//will unlock the current program

		void bindAtt(enum attID, char* Name);					//used to bind a attribute to a location
		void addAtt(char* Name);								//used to add an attribute location
		
		void addUniform(char* Name);							//uned to add a uniform attribute
		void addUniform(char* Name, bool  inVal);
		void addUniform(char* Name, int	  inVal);
		void addUniform(char* Name, float inVal);
		void addUniform(char* Name, const Vector2f &inVec);
		void addUniform(char* Name, const Vector3f &inVec);
		void addUniform(char* Name, const Vector4f &inVec);
		//void addUniform(char* Name, const mat2 &inMat);
		//void addUniform(char* Name, const mat3 &inMat);
		void addUniform(char* Name, const Mat4f &inMat);
		

		GLint getAtt(char* Name)		{return m_attributes[Name];	}	//obtains the attribute location
		GLint getUniform(char* Name)	{return m_uniforms[Name];	}	//obtains the uniform location

		void printActiveAtts();									//used to print the active attributes
		void printActiveUniform();								//used to print the active uniforms

		GLuint getProgram() {return m_program;}

	private:
		bool compileShader(const char* sSource, GLenum type);	//compiles a shader ready to be linked

	private:
		enum sType { MLS_GEOM = 0, MLS_VERT = 1, MLS_FRAG = 2}; // stores the shader types so that they can be reference easily in m_Shaders
		GLuint m_shaders[3];									//will store the 3 main shaders frag = 2, vert = 1 and geom = 0
		GLuint m_program;
		std::unordered_map<char*, int> m_attributes;			//stores the attribute locations. If BindAtt is used then the int will be associated with one of the predefined attributes in attID.
		std::unordered_map<char*, int> m_uniforms;				//stores the uniform locations


	};
}



//usage of this class is very similar to Movania's
//the shader is loaded first
//
// sClass->LoadShaderFile("shaders/vertex.vert, GL_VERTEX_SHADER);
//
//then the program is locked (created in the first case)
//
// sClass->lock(true); //true means it will create a program
//
//now any attributes can be added or at this point it may be best to bind them if you know them all
//
// sClass->bindAtt(ML_VERTEX_POS, "VertexPosition");
// sClass->addAtt("VertexPosition");
//
//uniforms can also be added
//
// sClass->addUniform("RotMat");
//
//then the shader can be linked one it has all attributes binded
//
// sClass->LinkShaders();
//
//the shader can now be unlocked (if needed)
//
// sClass->unlock();
//
//at any point the shader can be locked and unlocked by calling lock() (no need for true as it has already created the program) and unlock()
//
//so to render
//
// sClass->lock();
// /*	render	*/
// sClass->unlock();
// sClass2->lock()
// /*	render	*/
// sClass2->unlock();

