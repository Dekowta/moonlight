#include "WindowsHandler.h"
#include "ApplicationMain.h"
#include "general.h"
#pragma once

void CALLBACK fpsCounter(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);
void CALLBACK fpsCounter2(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);

int MLApp::frameCount = 0;


INT WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd )
{
	WindowsHandler*			winHand = new WindowsHandler(hInstance, hPrevInstance, lpCmdLine, nShowCmd);
	MLApp::ApplicationMain*	appMain = new MLApp::ApplicationMain(winHand);


	winHand->createWindow("Moonlight Renderer", 1280, 720, 32, true);
	appMain->initaliseApp();
	winHand->setTimer(1000, fpsCounter2);
	//winHand->setTimer(10, fpsCounter2);

// 	float a = 10.0f;
// 	float b = 10.0f;

// 	Vec2f* vector2Test = new Vec2f(a, b);
// 	char* MSF = new char[50];
// 	sprintf(MSF, "Vec2f x: %d y: %d", vector2Test->x, vector2Test->y);
// 	MLError::ErrorHandler::printError(MSF);
// 	free(MSF);	

	

	while(1)
	{
		winHand->update();
		appMain->updateApp();
	}

	Release(winHand);
	Release(appMain);

	return 1;
}

void CALLBACK fpsCounter(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
{
	MLApp::frameCount++;
}

void CALLBACK fpsCounter2(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2)
{
	char* MSF = new char[50];
	sprintf(MSF, "FPS: %d", MLApp::frameCount);
	WindowsHandler::setWindowText(MSF);
	free(MSF);	
	MLApp::frameCount = 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// This section will list all the current and finished tasks of 
// 
// OpenGL Tasks to do:
////////////////////////////////////////////////////////////////////////////////////////////////////

