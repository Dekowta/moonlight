#include "MLCoreGL.h"
#include "ErrorHandler.h"
#include <FreeImage.h>
#include <unordered_map>
#pragma once
namespace MLRenderer
{
	typedef struct TextureInfo
	{
		GLuint textureID;
		const char* Location;
		GLuint width;
		GLuint height;
	}textureInfo;


	class MLTextureManager
	{
	public:
		MLTextureManager(void);
		virtual ~MLTextureManager(void);

		static GLuint loadTexture(const char* path, const char* ID);
		static GLuint loadTexture(const char* path);

		static GLuint		getTexture(const char* ID)	{return TextureMap[ID].textureID;}
		static GLuint		getWidth(const char* ID)	{return TextureMap[ID].width;}
		static GLuint		getHeight(const char* ID)	{return TextureMap[ID].height;}
		static const char*	getPath(const char* ID)		{return TextureMap[ID].Location;}

		static GLuint		getWidth(GLuint ID)		{return TextureLocMap[ID].width;}
		static GLuint		getHeight(GLuint ID)	{return TextureLocMap[ID].height;}
		static const char*	getPath(GLuint ID)		{return TextureLocMap[ID].Location;}

	private:

		typedef std::unordered_map<const char*, MLRenderer::textureInfo> TexMap1;
		typedef std::unordered_map<GLuint, MLRenderer::textureInfo>	TexMap2;
		//There are 2 methods for storing texture Maps
		//Both should not be used at the same time! but they both perform a
		//similar job
		//TextureMap will store a string ID provided by the user and will store the GLuint
		//for that. to find a texture the user must pass the texture name allong with the location on load
		//and if the name matches any already created it will pass a GLuint back 
		static TexMap1 TextureMap;
		//TextureLocMap stores the GLuint as the UID and stores the texture location in the map
		//when the user loads another already loaded texture it will iterate through the map
		//and find the location and the key to go with it which will then be passed back
		static TexMap2 TextureLocMap;
	};
}


