#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#pragma once

using glm::mat4;
using glm::vec3;

namespace MLRenderer
{
	class MLCamera
	{
	public:
		MLCamera(void);
		virtual ~MLCamera(void);

		void create();
		void create(float x, float y);
		void create(float x, float y, float rot);

		void setCamPos(float x, float y);
		void setCamRot(float rot);

		mat4 getView() {return m_view;}
	private:
		mat4 m_view;
	};
}

