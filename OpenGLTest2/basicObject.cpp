#include "basicObject.h"

using MLRenderer::MLTextureManager;

basicObject::basicObject(void)
{
	ObjMat = glm::mat4(1.0f);
}


basicObject::~basicObject(void)
{
}


void basicObject::createOBJ()
{
	
		Shader	= new MLRenderer::MLShader();
		View	= new MLRenderer::MLCamera();

		View->create(0, 0);

		Shader->LoadShaderFile("shaders/basic.vert", GL_VERTEX_SHADER);
		Shader->LoadShaderFile("shaders/basic.frag", GL_FRAGMENT_SHADER);


		MLError::ErrorHandler::printGLError("Shader Lock");
		Shader->lock();
			MLError::ErrorHandler::printGLError("Bind Att");
			Shader->bindAtt(MLRenderer::ML_ATT_VERTEXCOL, "VertexColor");
			MLError::ErrorHandler::printGLError("Bind Att");
			Shader->bindAtt(MLRenderer::ML_ATT_VERTEXPOS, "VertexPosition");
			MLError::ErrorHandler::printGLError("Bind Att");
			Shader->bindAtt(MLRenderer::ML_ATT_TEXTURE0,	"TextureCoord");
			MLError::ErrorHandler::printGLError("Bind Att");
			Shader->LinkShaders();
			MLError::ErrorHandler::printGLError("Shader Link");
		Shader->unlock();
		MLError::ErrorHandler::printGLError("Shader unLock");
		//l = -400
		//r = -350
		//t = 300
		//b = 250

		glm::vec3 positionData[] = {
			vec3(0.0f,   0.0f, 0.0f),
			vec3(230.0f,   0.0f, 0.0f),
			vec3(230.0f,  230.0f, 0.0f),
			vec3(230.0f,  230.0f, 0.0f),
			vec3(0.0f,  230.0f, 0.0f),
			vec3(0.0f,   0.0f, 0.0f),
			vec3(60.0f,   60.0f, 0.0f),
			vec3(290.0f,   60.0f, 0.0f),
			vec3(290.0f,  290.0f, 0.0f),
			vec3(290.0f,  290.0f, 0.0f),
			vec3(60.0f,  290.0f, 0.0f),
			vec3(60.0f,   60.0f, 0.0f)};//4

		float colorData[] = {
			1.0f, 0.0f, 1.0f, //1
			1.0f, 0.0f, 1.0f, //2
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f, //1
			1.0f, 0.0f, 1.0f, //2
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f};//4

		float texturData[] = {
			0.0f, 1.0f,
			1.0f, 1.0f,
			1.0f, 0.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
			0.0f, 1.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,
			1.0f, 0.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
			0.0f, 1.0f
		};

		float index[] = {
			0, 1, 2, 
			3, 4, 5,
			6, 7, 8,
			9, 10, 11
		};

		

		// Create and populate the buffer objects
		GLuint vboHandles[4];
		glGenBuffers(4, vboHandles);

		GLuint positionBufferHandle = vboHandles[0];
		GLuint colorBufferHandle = vboHandles[1];
		GLuint textureBufferHandle = vboHandles[2];
		GLuint indexBufferHandle = vboHandles[3];

		float data = 0.1f;

		MLError::ErrorHandler::printGLError("Bind Buffers start");
		glBindBuffer(GL_ARRAY_BUFFER, positionBufferHandle);
		glBufferData(GL_ARRAY_BUFFER, 0 * sizeof(float), &data, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, colorBufferHandle);
		glBufferData(GL_ARRAY_BUFFER, 0 * sizeof(float), &data, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, textureBufferHandle);
		glBufferData(GL_ARRAY_BUFFER, 0 * sizeof(float), &data, GL_STATIC_DRAW);

		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferHandle);
		//glBufferData(GL_ELEMENT_ARRAY_BUFFER, 0 * sizeof(float), &data, GL_STATIC_DRAW);
		MLError::ErrorHandler::printGLError("Bind Buffers End");


		// Create and set-up the vertex array object
		glGenVertexArrays( 1, &vaoHandle );

		glBindVertexArray(vaoHandle);

		MLError::ErrorHandler::printGLError("Enable Vertex ATT Start ");
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXCOL);  // Vertex color
		glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
		MLError::ErrorHandler::printGLError("Enable Vertex ATT end");

		MLError::ErrorHandler::printGLError("Enable VAO end");
		glBindBuffer(GL_ARRAY_BUFFER, positionBufferHandle);
		glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );

		glBindBuffer(GL_ARRAY_BUFFER, colorBufferHandle);
		glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXCOL, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );

		glBindBuffer(GL_ARRAY_BUFFER, textureBufferHandle);
		glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );
		MLError::ErrorHandler::printGLError("Enable VAO end");

		MLError::ErrorHandler::printGLError("Bind Buffers2 start");
		glBindBuffer(GL_ARRAY_BUFFER, positionBufferHandle);
		glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float) * 3, positionData, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, colorBufferHandle);
		glBufferData(GL_ARRAY_BUFFER, 36 * sizeof(float), colorData, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, textureBufferHandle);
		glBufferData(GL_ARRAY_BUFFER, 24 * sizeof(float), texturData, GL_STATIC_DRAW);

		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferHandle);
		//glBufferData(GL_ELEMENT_ARRAY_BUFFER, 12 * sizeof(float), index, GL_STATIC_DRAW);
		MLError::ErrorHandler::printGLError("Bind Buffers2 start");

		//LoadTexture("textures/Test2.png");
		MLError::ErrorHandler::printGLError("BEFORE BIND");
		glBindTexture(GL_TEXTURE_2D, MLRenderer::MLTextureManager::loadTexture("textures/Test2.png"));
		MLError::ErrorHandler::printGLError("AFTER BIND");


		Shader->lock();
			Shader->addUniform("Tex1", 0);
		Shader->unlock();
}

void basicObject::renderObj()
{
	GLint indicies[] = {0, 6};
	GLsizei count[] = {6, 6};
	Shader->lock();
		Shader->addUniform("inMVP", (MLRenderer::MLCoreGL::getOrtho()));
		glBindVertexArray(vaoHandle);
		//glDrawArrays(GL_TRIANGLE_STRIP, 0, 12);
		const GLsizei amount = 6;
		glMultiDrawArrays(GL_TRIANGLE_STRIP, indicies, count, 2);
	Shader->unlock();
}

void basicObject::setPosition(float x, float y)
{
	ObjMat = glm::translate(mat4(1.0f), vec3(x, y, 0.0f));
}

void basicObject::LoadTexture(const char* filelocation)
{
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;

	FIBITMAP* fib(0);

	BYTE* bits(0);

	unsigned int width(0), height(0);

	GLuint texLoc;

	fif = FreeImage_GetFileType(filelocation);

	if (fif == FIF_UNKNOWN)
	{
		fif = FreeImage_GetFIFFromFilename(filelocation);
	}
	if (fif == FIF_UNKNOWN)
	{
		return;
	}


	if (FreeImage_FIFSupportsReading(fif))
	{
		fib = FreeImage_Load(fif, filelocation);
	}
	if (!fib)
	{
		return;
	}

	bits = FreeImage_GetBits(fib);
	width = FreeImage_GetWidth(fib);
	height = FreeImage_GetHeight(fib);

	if((bits == 0) || (width == 0) || (height == 0))
		return;

	int val = FreeImage_GetBPP(fib);

	//glActiveTexture(GL_TEXTURE0);

	glGenTextures(1, &texLoc);
	glBindTexture(GL_TEXTURE_2D, texLoc);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, bits);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	Shader->lock();
		Shader->addUniform("Tex1", 0);
	Shader->unlock();

	FreeImage_Unload(fib);

}