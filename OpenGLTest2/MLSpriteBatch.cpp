#include "MLSpriteBatch.h"

using MLRenderer::MLSpriteBatch;


MLSpriteBatch::MLSpriteBatch(void)
{
	m_shader = new MLShader();
	m_shader->LoadShaderFile("shaders/SBatch.vert", GL_VERTEX_SHADER);
	m_shader->LoadShaderFile("shaders/SBatch.frag", GL_FRAGMENT_SHADER);
	m_shader->lock();
	{
		m_shader->bindAtt(MLRenderer::ML_ATT_VERTEXPOS, "VertexPosition");
		m_shader->bindAtt(MLRenderer::ML_ATT_TEXTURE0,	"TextureCoord");
		m_shader->LinkShaders();
		MLError::ErrorHandler::printGLError(BatchError"LINK SHADERS");
		MLError::ErrorHandler::printGLError(BatchError"BEFORE UNLOCK");
	}
	m_shader->unlock();
	MLError::ErrorHandler::printGLError(BatchError"AFTER UNLOCK");
	m_beginCall = false;
	Initalise();
}

MLSpriteBatch::MLSpriteBatch(bool Multi)
{
	m_shader = new MLShader();
	m_shader->LoadShaderFile("shaders/SBatch.vert", GL_VERTEX_SHADER);
	m_shader->LoadShaderFile("shaders/SBatch.frag", GL_FRAGMENT_SHADER);
	m_shader->lock();
	{
		m_shader->bindAtt(MLRenderer::ML_ATT_VERTEXPOS, "VertexPosition");
		m_shader->bindAtt(MLRenderer::ML_ATT_TEXTURE0,	"TextureCoord");
		m_shader->LinkShaders();
		MLError::ErrorHandler::printGLError(BatchError"LINK SHADERS");
		MLError::ErrorHandler::printGLError(BatchError"BEFORE UNLOCK");
	}
	m_shader->unlock();
	MLError::ErrorHandler::printGLError(BatchError"AFTER UNLOCK");
	m_beginCall = false;
	
	if (Multi)
		InitaliseMulti();
	else
		Initalise();
	
	
}

MLSpriteBatch::MLSpriteBatch(MLShader* inShader) : m_shader(inShader) 
{
	m_beginCall = false;
	Initalise();
}

MLSpriteBatch::~MLSpriteBatch(void)
{
}

void MLSpriteBatch::Initalise()
{
	MLError::ErrorHandler::printGLError(BatchError"BEFORE GEN BUFFERS");
	GLuint buffID[2];
	glGenBuffers(2, buffID);
	MLError::ErrorHandler::printGLError(BatchError"AFTER GEN BUFFERS");

	m_VposBuffer	= buffID[0];
	m_UVBuffer		= buffID[1];

	glBindBuffer(GL_ARRAY_BUFFER, m_VposBuffer);
	//MLError::ErrorHandler::printGLError(BatchError"BIND V BUFFER");
	glBufferData(GL_ARRAY_BUFFER, (ML_MAX_SPRITES * 6) * sizeof(Vector3f), 0,  GL_DYNAMIC_DRAW);
	//MLError::ErrorHandler::printGLError(BatchError"V BUFF DATA");
	glBindBuffer(GL_ARRAY_BUFFER, m_UVBuffer);
	//MLError::ErrorHandler::printGLError(BatchError"BIND T BUFFER");
	glBufferData(GL_ARRAY_BUFFER, (ML_MAX_SPRITES * 6) * sizeof(Vector3f), 0,  GL_DYNAMIC_DRAW);
	
	glGenVertexArrays(1, &m_BatchVAO);
	//MLError::ErrorHandler::printGLError(BatchError"GEN VAO");

	glBindVertexArray(m_BatchVAO);
	//MLError::ErrorHandler::printGLError(BatchError"BIND VAO");
	glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
	glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
	//MLError::ErrorHandler::printGLError(BatchError"ENABLE ATT");

	glBindBuffer(GL_ARRAY_BUFFER, m_VposBuffer);
	//MLError::ErrorHandler::printGLError(BatchError"BIND V BUFF");
	glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );
	//MLError::ErrorHandler::printGLError(BatchError"SET V BUFF ATT");

	glBindBuffer(GL_ARRAY_BUFFER, m_UVBuffer);
	//MLError::ErrorHandler::printGLError(BatchError"BIND T BUFF");
	glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, (GLubyte *)NULL );
	//MLError::ErrorHandler::printGLError(BatchError"SET T BUFF ATT");

	//glDisableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);
	//glDisableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);
	//MLError::ErrorHandler::printGLError(BatchError"UNBIND ATT");
	
	glBindVertexArray(0);
	//MLError::ErrorHandler::printGLError(BatchError"UNBIND VAO");
}

void MLSpriteBatch::InitaliseMulti()
{
	MLError::ErrorHandler::printGLError(BatchError"BEFORE GEN BUFFERS");
	GLuint buffID;
	glGenBuffers(1, &buffID);
	MLError::ErrorHandler::printGLError(BatchError"AFTER GEN BUFFERS");

	m_MultiBuffer	= buffID;
	

	glBindBuffer(GL_ARRAY_BUFFER, m_MultiBuffer);
	//MLError::ErrorHandler::printGLError(BatchError"BIND V & T BUFFER");
	glBufferData(GL_ARRAY_BUFFER, (ML_MAX_SPRITES * 6) * sizeof(Vector3f) * 2, 0, GL_DYNAMIC_DRAW);
	
	glGenVertexArrays(1, &m_BatchVAO);
	//MLError::ErrorHandler::printGLError(BatchError"GEN VAO");

	glBindVertexArray(m_BatchVAO);
	//MLError::ErrorHandler::printGLError(BatchError"BIND VAO");
	glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);  // Vertex position
	glEnableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);	//vertex texture coord
	//MLError::ErrorHandler::printGLError(BatchError"ENABLE ATT");

	glBindBuffer(GL_ARRAY_BUFFER, m_MultiBuffer);
	//MLError::ErrorHandler::printGLError(BatchError"BIND V BUFF");
	glVertexAttribPointer(MLRenderer::ML_ATT_VERTEXPOS, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3f) * 2, (GLubyte *)NULL );
	//MLError::ErrorHandler::printGLError(BatchError"SET V BUFF ATT");
	//MLError::ErrorHandler::printGLError(BatchError"BIND T BUFF");
	glVertexAttribPointer(MLRenderer::ML_ATT_TEXTURE0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3f) * 2, (GLubyte *)sizeof(Vector3f));
	//MLError::ErrorHandler::printGLError(BatchError"SET T BUFF ATT");

	//glDisableVertexAttribArray((GLuint)MLRenderer::ML_ATT_VERTEXPOS);
	//glDisableVertexAttribArray((GLuint)MLRenderer::ML_ATT_TEXTURE0);
	//MLError::ErrorHandler::printGLError(BatchError"UNBIND ATT");

	glBindVertexArray(0);
	//MLError::ErrorHandler::printGLError(BatchError"UNBIND VAO");
}


void MLSpriteBatch::Begin(bool Alpha /* = true */)
{
	if (Alpha)
	{
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
		glDisable(GL_BLEND);

	m_beginCall = true;
}

void MLSpriteBatch::Draw(MLSprite* Sprite, bool Pack)
{
	if (!Sprite->getEnabled())
		return;

	bool NewBatch = false;

	MLBatchItem::batchInfo* tmpBatch;

	BatchMap::iterator it = m_BatchItems.find(Sprite->getTexture());
	if (it != m_BatchItems.end())
		tmpBatch = m_BatchItems[Sprite->getTexture()];
	else
	{
		tmpBatch = new MLBatchItem::batchInfo();
		tmpBatch->spriteCount = 0;
		NewBatch = true;
	}

	Vector3f topLeft		= Vector3f(Sprite->getPos().x, Sprite->getPos().y , 0.0f);
	Vector3f topRight		= Vector3f((Sprite->getPos().x + Sprite->getSize().x), Sprite->getPos().y, 0.0f);
	Vector3f bottmLeft		= Vector3f(Sprite->getPos().x, (Sprite->getPos().y + Sprite->getSize().y), 0.0f);
	Vector3f bottomRight	= Vector3f((Sprite->getPos().x + Sprite->getSize().x), (Sprite->getPos().y + Sprite->getSize().y), 0.0f);

	if (Pack)
	{
		tmpBatch->addPointMulti(topLeft, Vector3f(0.0f, 1.0f, 0.0f));
		tmpBatch->addPointMulti(topRight, Vector3f(1.0f, 1.0f, 0.0f));
		tmpBatch->addPointMulti(bottomRight, Vector3f(1.0f, 0.0f, 0.0f));
		tmpBatch->addPointMulti(bottmLeft, Vector3f(0.0f, 0.0f, 0.0f));
	}
	else
	{
		tmpBatch->addPoint(topLeft, Vector2f(0.0f, 1.0f));
		tmpBatch->addPoint(topRight, Vector2f(1.0f, 1.0f));
		tmpBatch->addPoint(bottomRight, Vector2f(1.0f, 0.0f));
		tmpBatch->addPoint(bottmLeft, Vector2f(0.0f, 0.0f));
	}
	
	

	tmpBatch->spriteCount++;

	if (NewBatch)
		m_BatchItems[Sprite->getTexture()] = tmpBatch;

}

void MLSpriteBatch::Draw(GLuint Texture, Vector2f Pos, Vector2f Size)
{
	
	bool NewBatch = false;

	MLBatchItem::batchInfo* tmpBatch;

	BatchMap::iterator it = m_BatchItems.find(Texture);
	if (it != m_BatchItems.end())
		tmpBatch = m_BatchItems[Texture];
	else
	{
		tmpBatch = new MLBatchItem::batchInfo();
		tmpBatch->spriteCount = 0;
		NewBatch = true;
	}

	Vector3f topLeft		= Vector3f(Pos.x, Pos.y , 0.0f);
	Vector3f topRight		= Vector3f((Pos.x + Size.x), Pos.y, 0.0f);
	Vector3f bottmLeft		= Vector3f(Pos.x, (Pos.y + Size.y), 0.0f);
	Vector3f bottomRight	= Vector3f((Pos.x + Size.x), (Pos.y + Size.y), 0.0f);

	tmpBatch->addPoint(topLeft, Vector2f(0.0f, 1.0f));
	tmpBatch->addPoint(topRight, Vector2f(1.0f, 1.0f));
	tmpBatch->addPoint(bottomRight, Vector2f(1.0f, 0.0f));

	tmpBatch->addPoint(bottomRight, Vector2f(1.0f, 0.0f));
	tmpBatch->addPoint(bottmLeft, Vector2f(0.0f, 0.0f));
	tmpBatch->addPoint(topLeft, Vector2f(0.0f, 1.0f));

	tmpBatch->spriteCount++;

	if (NewBatch)
		m_BatchItems[Texture] = tmpBatch;

}

void MLSpriteBatch::End(bool Multi)
{
	if (!m_beginCall)
	{
		MLError::ErrorHandler::printError("Begin must be called before end");
		return;
	}
	//sorting
	if (Multi)
		RenderMulti();
	else
		Render();
	
	
	m_BatchItems.clear();
}

void MLSpriteBatch::Render()
{

	m_shader->lock();
	//MLError::ErrorHandler::printGLError(BatchError"BEFORE UNIFORM");
	m_shader->addUniform("inMVP", (MLRenderer::MLCoreGL::getOrtho()));
	//MLError::ErrorHandler::printGLError(BatchError"MAT");
	m_shader->addUniform("Tex1", 0);
	//MLError::ErrorHandler::printGLError(BatchError"TEX");

	BatchMap::iterator it = m_BatchItems.begin();
	for(; it != m_BatchItems.end(); it++)
	{
		MLBatchItem::batchInfo* currentBatch = it->second;

		//MLError::ErrorHandler::printGLError(BatchError"BEFORE BIND");
		glBindTexture(GL_TEXTURE_2D, it->first);
		//MLError::ErrorHandler::printGLError(BatchError"AFTER BIND");
		glBindBuffer(GL_ARRAY_BUFFER, m_VposBuffer);
		//MLError::ErrorHandler::printGLError(BatchError"BIND V BUFFER");
		glBufferSubData(GL_ARRAY_BUFFER, 0, currentBatch->spriteCount * 4 * sizeof(Vector3f), currentBatch->VPos);
		//MLError::ErrorHandler::printGLError(BatchError"V BUFF DATA");
		glBindBuffer(GL_ARRAY_BUFFER, m_UVBuffer);
		//MLError::ErrorHandler::printGLError(BatchError"BIND T BUFFER");
		glBufferSubData(GL_ARRAY_BUFFER, 0, currentBatch->spriteCount * 4 * sizeof(Vector2f), currentBatch->UV);
		//MLError::ErrorHandler::printGLError(BatchError"T BUFF DATA");

		GLint* indicies = new GLint[currentBatch->spriteCount]();
		GLsizei* Vcount = new GLsizei[currentBatch->spriteCount]();

		for(int i = 0; i < currentBatch->spriteCount; i++)
		{
			indicies[i] = i * 4;
			Vcount[i] = 4;
		}

		glBindVertexArray(m_BatchVAO);
		//MLError::ErrorHandler::printGLError(BatchError"BIND VAO");
		glMultiDrawArrays(GL_QUADS, indicies, Vcount, currentBatch->spriteCount);
		//MLError::ErrorHandler::printGLError(BatchError"RENDER");
		glBindVertexArray(0);
		//MLError::ErrorHandler::printGLError(BatchError"UNBIND VAO");
		glBindTexture(GL_TEXTURE_2D, 0);
		//MLError::ErrorHandler::printGLError(BatchError"UNBIND TEXTURE");

		delete [] indicies;
		delete [] Vcount;
		delete it->second;

	}
	m_shader->unlock();


}

void MLSpriteBatch::RenderMulti()
{

	m_shader->lock();
	//MLError::ErrorHandler::printGLError(BatchError"BEFORE UNIFORM");
	m_shader->addUniform("inMVP", (MLRenderer::MLCoreGL::getOrtho()));
	//MLError::ErrorHandler::printGLError(BatchError"MAT");
	m_shader->addUniform("Tex1", 0);
	//MLError::ErrorHandler::printGLError(BatchError"TEX");

	BatchMap::iterator it = m_BatchItems.begin();
	for(; it != m_BatchItems.end(); it++)
	{
		MLBatchItem::batchInfo* currentBatch = it->second;

		//MLError::ErrorHandler::printGLError(BatchError"BEFORE BIND");
		glBindTexture(GL_TEXTURE_2D, it->first);
		//MLError::ErrorHandler::printGLError(BatchError"AFTER BIND");
		glBindBuffer(GL_ARRAY_BUFFER, m_MultiBuffer);
		//MLError::ErrorHandler::printGLError(BatchError"BIND V BUFFER");
		glBufferSubData(GL_ARRAY_BUFFER, 0, (currentBatch->spriteCount * 4) * (sizeof(Vector3f) * 2), currentBatch->Multi);
		//MLError::ErrorHandler::printGLError(BatchError"V BUFF DATA");
		

		GLint* indicies = new GLint[currentBatch->spriteCount]();
		GLsizei* Vcount = new GLsizei[currentBatch->spriteCount]();

		for(int i = 0; i < currentBatch->spriteCount; i++)
		{
			indicies[i] = i * 4;
			Vcount[i] = 4;
		}

		glBindVertexArray(m_BatchVAO);
		//MLError::ErrorHandler::printGLError(BatchError"BIND VAO");
		glMultiDrawArrays(GL_QUADS, indicies, Vcount, currentBatch->spriteCount);
		//MLError::ErrorHandler::printGLError(BatchError"RENDER");
		glBindVertexArray(0);
		//MLError::ErrorHandler::printGLError(BatchError"UNBIND VAO");
		glBindTexture(GL_TEXTURE_2D, 0);
		//MLError::ErrorHandler::printGLError(BatchError"UNBIND TEXTURE");

		delete [] indicies;
		delete [] Vcount;
		delete it->second;

	}
	m_shader->unlock();


}

