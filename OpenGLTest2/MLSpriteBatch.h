#include "MLSprite.h"
#include "MLShader.h"
#include <glm/glm.hpp>
#include <unordered_map>
#include <map>
#include <vector>
#pragma once

using MLRenderer::MLSprite;
using MLRenderer::MLShader;
using namespace MLMath::MLVector2;
using namespace MLMath::MLVector3;

#define ML_MAX_SPRITES 10000
#define BatchError "[SBatch] "

namespace MLRenderer
{
	namespace MLBatchItem
	{
		typedef struct batchInfo
		{
			Vector3f VPos[ML_MAX_SPRITES * 4];
			Vector2f UV[ML_MAX_SPRITES * 4];
			Vector3f Multi[ML_MAX_SPRITES * 8];
			batchInfo() : pointcount(0), spriteCount(0){}

			void addPoint(Vector3f Vpos, Vector2f uv)
			{
				VPos[pointcount] = Vpos;
				UV[pointcount] = uv;
				pointcount++;
			}

			void addPointMulti(Vector3f Vpos, Vector3f uv)
			{
				Multi[pointcount] = Vpos;
				pointcount++;
				Multi[pointcount] = uv;
				pointcount++;
			}
			int pointcount;
			int spriteCount;			
		}batchInfo;

	}

	class MLSpriteBatch
	{
	public:
		MLSpriteBatch(void);
		MLSpriteBatch(bool Multi);
		MLSpriteBatch(MLShader* inShader);
		virtual ~MLSpriteBatch(void);

		void Begin(/*Settings*/bool Alpha = true);
		void Draw(MLSprite* Sprite, bool Pack=false);
		void Draw(GLuint Texture, Vector2f Pos, Vector2f Size);
		void End(bool Multi = false);
	private:
		void Render();
		void RenderMulti();
		void Initalise();
		void InitaliseMulti();
		
	private:
		bool m_beginCall;

		GLuint m_VposBuffer;
		GLuint m_UVBuffer;
		GLuint m_MultiBuffer;
		GLuint m_BatchVAO;

		MLShader* m_shader;
		
		typedef std::map<GLuint, MLBatchItem::batchInfo*> BatchMap;
		BatchMap m_BatchItems;
	};
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Notes about the spritebatcher
// 
// The idea of the spritebatch is to create a way to manage and deal with rendering without
// creating multiple VBO and have the ability to do some intresting stuff when drawing the sprites
// 
// There are going to be 3 major stages of the sprite batch
// 
// 1 - spritebatch->begin(settings)  
//	 - This function will prep the sprite batch with a few settings on the method of drawing   
//	 - maybe camera to use blending states projection type shader program to use to render  
//	 - the list can go on. So any inialisation should be done here  
// 
// 2 - spritebatch->draw(spriteinfo)  
//   - There are 2 methods to do this, the first is to have a few functions which take the texture  
//   - position and any data needed about the sprite. That data can then be used to create the sprite  
//   - The second which is the method I would rather like to use is to have a separate  
//   - sprite object which will store information about the sprite  
//   - this can then be passed to draw and it will be organized and added to the drawing  
//   - it is also means an instance of the sprite exists rather than creating when you want to draw .  
//   - both functions can exist though .  
//   
// 3 - spritebatch->end()  
//   - This is the final step of the sprite render and should only be called when everything  
//   - has been set to render as this will start the render process. Using all the sprite  
//   - data that has been passed to it from when begin was called.  
//   - 
//   - This may also be a good function to use on another thread as a new batch could be set up while  
//   - it is processing the rendering  
//   
//   
// Useful Links:
//  - openGL ES			  - http://craiggiles.wordpress.com/2009/08/03/opengl-es-batch-rendering-on-the-iphone/  
//  - MonoGame XNA clone  - https://github.com/mono/MonoGame/blob/master/MonoGame.Framework/Graphics/SpriteBatch.cs  
//  - Love2D sprite batch - https://bitbucket.org/rude/love/src/06e20ce294cd/src/modules/graphics/opengl/SpriteBatch.cpp
// 
// 
// 
// 18/08/2012
// There seem to be many different ways to do this and I will do the following. Create a sprite object with some information
// and then that can be sent to the spritebatch. If its the same as the previous it will draw it in the new position
// the sprite texture will be the main check.
// 
// The Idea is to prevent multiple data from being rendered. a Batch will be created which will be rendered once end is called
// this may be rather taxing and will need some performance tests to make it as fast as possible
// 
// I need to look into modifying or storing multiple vertex buffers 
// 
// the main data that can be modified without change is the position and rotation of the sprite as well as scale (possibly x and y);
// if any other data changed then it must be stored ready for rendering.
// 
// Useful Links:
//  - Sprite batch from some guy on gamedev	- http://www.gamedev.net/topic/610277-why-vbo-dosent-help-much-for-sprites/  
//											  http://pastebin.com/BpU8HfAV , http://pastebin.com/5kn4vC2P
//	- texture buffer?						- http://www.opengl.org/wiki/GLSL_Samplers#Direct_texel_fetches , http://www.opengl.org/wiki/Buffer_Texture , http://www.gamedev.net/topic/607838-sprite-blitting/
//	                                        
//	also note use ms per frame as its a better method of recording performance. check the gamedev thread
// 
// 
// So this is the way im going to go about this. Im going to create a DrawElement method with my VBO and populate it with vertexdata texture
// what this means is that sprite1 has texture A and sprite2 also has Texture A however both have different positions.
// 
// when the draw method is called with each sprite it knows the textures are the same and populates a vertex array and texture coord array
// with the values (making sure there is space between them). once end has been called it will look through all the textures
// that have been sent to the spritebatch and will then map the texture, vertex coord and texture coords call drawelements and then look 
// for the next texture in the array and repeat the process.
// 
// for a bullet hell game all the bullet designs could be placed in one texture and use the source rectangle to map
// the texutre to the vertex data. with one draw call for all the bullets is would be super quick.
// 
// for other games it may not be so great but using texture atlesing may help with some areas (maybe atles level assets when creating it); 
// 
// 
////////////////////////////////////////////////////////////////////////////////////////////////////

