#version 400
in vec3 VertexPosition;
in vec2 TextureCoord;

out vec2 TexCoord;

uniform mat4 inMVP;

void main()
{

	TexCoord = TextureCoord;

	//gl_Position = (Projection * pos);
	gl_Position = inMVP * vec4( VertexPosition, 1.0f); 
	//gl_Position = vec4( VertexPosition, 1.0f);
}