#include "MLTextureManager.h"

using MLRenderer::MLTextureManager;


typedef std::unordered_map<const char*, MLRenderer::textureInfo> TexMap1;
typedef std::unordered_map<GLuint, MLRenderer::textureInfo>	TexMap2;

TexMap1  MLTextureManager::TextureMap;
TexMap2  MLTextureManager::TextureLocMap;

MLTextureManager::MLTextureManager(void)
{
}


MLTextureManager::~MLTextureManager(void)
{
}


GLuint MLTextureManager::loadTexture(const char* path)
{
	if (!TextureMap.empty())
		MLError::ErrorHandler::printError("TEXTUREMANAGER: Warning both textureMaps being used!");

	//loop through the texture map and see if the texture is already loaded if so return the GLuint for it
	TexMap2::iterator it = TextureLocMap.begin();
	for(;it != TextureLocMap.end(); it++)
	{
		if(it->second.Location == path)
			return it->first;
	}


	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	FIBITMAP* fib(0);
	BYTE* bits(0);

	unsigned int width(0), height(0);

	GLuint texLoc;

	fif = FreeImage_GetFileType(path);

	if (fif == FIF_UNKNOWN)
		fif = FreeImage_GetFIFFromFilename(path);
	if (fif == FIF_UNKNOWN)
		return -1;

	if (FreeImage_FIFSupportsReading(fif))
		fib = FreeImage_Load(fif, path);
	if (!fib)
		return -1;

	bits = FreeImage_GetBits(fib);
	width = FreeImage_GetWidth(fib);
	height = FreeImage_GetHeight(fib);

	if((bits == 0) || (width == 0) || (height == 0))
		return -1;

	//glActiveTexture(GL_TEXTURE0);
	
	glGenTextures(1, &texLoc);
	MLError::ErrorHandler::printGLError("TEXTUREMANAGER: glGenTextures");
	glBindTexture(GL_TEXTURE_2D, texLoc);
	MLError::ErrorHandler::printGLError("TEXTUREMANAGER: glBindTexture");
	switch(fif)
	{
		case FIF_JPEG:
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, bits);
			break;
		case FIF_PNG:
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, bits);
			break;
	}
	
	MLError::ErrorHandler::printGLError("TEXTUREMANAGER: glTexImage2D");
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	MLError::ErrorHandler::printGLError("TEXTUREMANAGER: glTexParameterf");
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	MLError::ErrorHandler::printGLError("TEXTUREMANAGER: glTexParameterf");
	glBindTexture(GL_TEXTURE_2D, 0);

	FreeImage_Unload(fib);

	TextureLocMap[texLoc].Location	= path;
	TextureLocMap[texLoc].textureID = texLoc;
	TextureLocMap[texLoc].width		= width;
	TextureLocMap[texLoc].height	= height;

	return texLoc;
}

GLuint MLTextureManager::loadTexture(const char* path, const char* ID)
{
	if (!TextureLocMap.empty())
		MLError::ErrorHandler::printError("TEXTUREMANAGER: Warning both textureMaps being used!");

	TexMap1::iterator it = TextureMap.find(ID);
	if(it != TextureMap.end())
		return it->second.textureID;

	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	FIBITMAP* fib(0);
	BYTE* bits(0);

	unsigned int width(0), height(0);

	GLuint texLoc;

	fif = FreeImage_GetFileType(path);

	if (fif == FIF_UNKNOWN)
		fif = FreeImage_GetFIFFromFilename(path);
	if (fif == FIF_UNKNOWN)
		return -1;

	if (FreeImage_FIFSupportsReading(fif))
		fib = FreeImage_Load(fif, path);
	if (!fib)
		return -1;

	bits = FreeImage_GetBits(fib);
	width = FreeImage_GetWidth(fib);
	height = FreeImage_GetHeight(fib);

	if((bits == 0) || (width == 0) || (height == 0))
		return -1;

	//glActiveTexture(GL_TEXTURE0);
	MLError::ErrorHandler::printGLError("TEXTUREMANAGER: glGenTextures");
	glGenTextures(1, &texLoc);
	glBindTexture(GL_TEXTURE_2D, texLoc);
	MLError::ErrorHandler::printGLError("TEXTUREMANAGER: glTexImage2D");
	switch(fif)
	{
	case FIF_JPEG:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, bits);
		break;
	case FIF_PNG:
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, bits);
		break;
	}

	MLError::ErrorHandler::printGLError("TEXTUREMANAGER: glTexParameterf");
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	MLError::ErrorHandler::printGLError("TEXTUREMANAGER: glTexParameterf");
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	FreeImage_Unload(fib);


	TextureMap[ID].Location		= path;
	TextureMap[ID].textureID	= texLoc;
	TextureMap[ID].width		= width;
	TextureMap[ID].height		= height;

	return texLoc;
}