#ifndef WINDOWSHANDLER_H
#define WINDOWSHANDLER_H

#include "MLCoreGL.h"
#include "ErrorHandler.h"
#include <windows.h>
#include <WindowsX.h>
#include <MMSystem.h>
#include <io.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <fcntl.h>
#pragma once

#define MAX_CONSOLE_LINES 500;
#define WINHAND "[Windows Handler] "

class WindowsHandler
{
public:
	WindowsHandler(void);
	WindowsHandler(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);
	virtual ~WindowsHandler(void);

	//creates a window with a specified width and height
	bool createWindow(LPCTSTR title, int width, int height, int BitCount, bool Console);
	
	bool setTimer(UINT Amount, LPTIMECALLBACK TimeProgram);

	bool update();
	bool release();

	static void setWindowText(LPCTSTR Text) {SetWindowText(_hWnd, Text);}
private:

	void loadConsole();

public:
	//These should be the only public variables used within the whole application
	static HWND		_hWnd;
	static int		_wHeight;
	static int		_wWidth;
	static bool		_FullScreen;
	static int		_BitMode;
	MSG				m_msg;

private:
	HINSTANCE		m_hInstance;
	HINSTANCE		m_hPrevInstance;
	LPSTR			m_lpCmdLine;
	int				m_nCmdShow;
	LPSTR			m_clsName;

	UINT			m_nanoTimer;

};

#endif //WINDOWSHANDLER_H