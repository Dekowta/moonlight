#ifndef MLMATH_H
#define MLMATH_H

#include "MLMath/Vector/MLVector2.h"
#include "MLMath/Vector/MLVector3.h"
#include "MLMath/Vector/MLVector4.h"

#include "MLMath/Matrix/MLMat4.h"

#include "MLMath/Matrix/MLMatFunc.h"

#endif