#ifndef MLSTANDARDMATH_H
#define MLSTANDARDMATH_H

#include "MLMath/MLMathDefine.h"
#include <math.h>
#define ML_PI 3.1415926536897932384626
#define ML_Rad2Deg(r) (r * (180/ML_PI))
#define ML_Deg2Rad(d) (d * (ML_PI/180))

namespace MLMath
{
	namespace MLStandard
	{
		ML_MATH_INLINE int factorial(int x);

		ML_MATH_INLINE int factorial(int x)
		{
			return (x == 1 ? x : x * factorial(x - 1));
		}
	}
}
#endif