#ifndef MLMATFUNC_H
#define MLMATFUNC_H
#include <math.h>
#include "MLMath/MLMathDefine.h"

#include "MLMath/Matrix/MLMat4.h"

#include "MLMath/Vector/MLVector3.h"

namespace MLMath
{
	namespace MLMat4
	{
		void identify(Mat4f& inMat);
		void translate(Mat4f& inMat, MLVector3::Vector3f pos);
		void scale(Mat4f& inMat, MLVector3::Vector3f XYZScale);
		void rotation(Mat4f& inMat, float rotX, float rotY, float rotZ);
		void ortho(Mat4f& inMat, float top, float bottom, float left, float right, float nearz, float farz);

		ML_MATH_INLINE void identify(Mat4f& inMat)
		{
			inMat.m[0][0] = 1.0f;
			inMat.m[0][1] = 0.0f;
			inMat.m[0][2] = 0.0f;
			inMat.m[0][3] = 0.0f;

			inMat.m[1][0] = 0.0f;
			inMat.m[1][1] = 1.0f;
			inMat.m[1][2] = 0.0f;
			inMat.m[1][3] = 0.0f;

			inMat.m[2][0] = 0.0f;
			inMat.m[2][1] = 0.0f;
			inMat.m[2][2] = 1.0f;
			inMat.m[2][3] = 0.0f;

			inMat.m[3][0] = 0.0f;
			inMat.m[3][1] = 0.0f;
			inMat.m[3][2] = 0.0f;
			inMat.m[3][3] = 1.0f;
		}

		ML_MATH_INLINE void translate(Mat4f& inMat, MLVector3::Vector3f pos)
		{
			inMat.m[0][0] = 1.0f;
			inMat.m[0][1] = 0.0f;
			inMat.m[0][2] = 0.0f;
			inMat.m[0][3] = pos.x;

			inMat.m[1][0] = 0.0f;
			inMat.m[1][1] = 1.0f;
			inMat.m[1][2] = 0.0f;
			inMat.m[1][3] = pos.y;

			inMat.m[2][0] = 0.0f;
			inMat.m[2][1] = 0.0f;
			inMat.m[2][2] = 1.0f;
			inMat.m[2][3] = pos.z;

			inMat.m[3][0] = 0.0f;
			inMat.m[3][1] = 0.0f;
			inMat.m[3][2] = 0.0f;
			inMat.m[3][3] = 1.0f;
		}

		ML_MATH_INLINE void scale(Mat4f& inMat, MLVector3::Vector3f XYZScale)
		{
			inMat.m[0][0] = XYZScale.x;
			inMat.m[0][1] = 0.0f;
			inMat.m[0][2] = 0.0f;
			inMat.m[0][3] = 0.0f;

			inMat.m[1][0] = 0.0f;
			inMat.m[1][1] = XYZScale.y;
			inMat.m[1][2] = 0.0f;
			inMat.m[1][3] = 0.0f;

			inMat.m[2][0] = 0.0f;
			inMat.m[2][1] = 0.0f;
			inMat.m[2][2] = XYZScale.z;
			inMat.m[2][3] = 0.0f;

			inMat.m[3][0] = 0.0f;
			inMat.m[3][1] = 0.0f;
			inMat.m[3][2] = 0.0f;
			inMat.m[3][3] = 1.0f;
		}

		ML_MATH_INLINE void rotation(Mat4f& inMat, float rotX, float rotY, float rotZ)
		{
			float cosY = cos(rotY);
			float cosZ = cos(rotZ);
			float cosX = cos(rotX);
			float sinY = sin(rotY);
			float sinZ = sin(rotZ);
			float sinX = sin(rotX);

			inMat.m[0][0] = (cosY * cosZ);
			inMat.m[0][1] = ((cosZ * sinX * sinY) - (cosX * sinZ));
			inMat.m[0][2] = ((cosX * cosZ * sinY) + (sinX * sinZ));
			inMat.m[0][3] = 0.0f;

			inMat.m[1][0] = (cosY * sinZ);
			inMat.m[1][1] = ((cosX * cosZ) + (sinX * sinY * sinZ));
			inMat.m[1][2] = -((cosZ * sinX) + (cosX * sinY * sinZ));
			inMat.m[1][3] = 0.0f;

			inMat.m[2][0] = -(sinY);
			inMat.m[2][1] = (cosY * sinX);
			inMat.m[2][2] = (cosX * cosY);
			inMat.m[2][3] = 0.0f;

			inMat.m[3][0] = 0.0f;
			inMat.m[3][1] = 0.0f;
			inMat.m[3][2] = 0.0f;
			inMat.m[3][3] = 1.0f;
		}

		ML_MATH_INLINE void ortho(Mat4f& inMat, float top, float bottom, float left, float right, float nearz, float farz)
		{
			inMat.m[0][0] = 2 / (right - left);
			inMat.m[0][1] = 0.0f;
			inMat.m[0][2] = 0.0f;
			inMat.m[0][3] = 0.0f;

			inMat.m[1][0] = 0.0f;
			inMat.m[1][1] = 2 / (top - bottom);
			inMat.m[1][2] = 0.0f;
			inMat.m[1][3] = 0.0f;

			inMat.m[2][0] = 0.0f;
			inMat.m[2][1] = 0.0f;
			inMat.m[2][2] = -(2 / (farz - nearz));
			inMat.m[2][3] = 0.0f;

			inMat.m[3][0] = -((right + left) / (right - left));
			inMat.m[3][1] = -((top + bottom) / (top - bottom));
			inMat.m[3][2] = -((farz + nearz) / (farz - nearz));
			inMat.m[3][3] = 1.0f;
		}
	}
}

#endif