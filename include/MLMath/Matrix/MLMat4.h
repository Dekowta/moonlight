#ifndef MLMAT4_H
#define MLMAT4_H
#include "MLMathDefine.h"
#include "MLStandardMath.h"

namespace MLMath
{
	namespace MLMat4
	{
		struct Mat4f
		{
			float m[4][4];

			Mat4f();
			Mat4f(float val);
			Mat4f(float** val);
			Mat4f(float m00, float m01, float m02, float m03,
				  float m10, float m11, float m12, float m13,
				  float m20, float m21, float m22, float m23,
				  float m30, float m31, float m32, float m33);

			Mat4f& operator=(const Mat4f &inVec);
			Mat4f& operator=(const float &infloat);
			Mat4f& operator+=(const Mat4f &inVec);
			Mat4f& operator-=(const Mat4f &inVec);
			Mat4f& operator*=(const Mat4f &inVec);
			Mat4f& operator*=(const float &infloat);
			Mat4f& operator/=(const float &infloat);
			Mat4f operator+(const Mat4f &inVec);
			Mat4f operator-(const Mat4f &inVec);
			Mat4f operator*(const Mat4f &inVec);
			Mat4f operator*(const float &infloat);
			Mat4f operator/(const float &infloat);

		};

		float* toptr(const Mat4f &Mat4A);

				
		//constructors
		//================================================================
		ML_MATH_INLINE Mat4f::Mat4f()
		{
			m[0][0] = 1.0f;
			m[0][1] = 0.0f;
			m[0][2] = 0.0f;
			m[0][3] = 0.0f;

			m[1][0] = 0.0f;
			m[1][1] = 1.0f;
			m[1][2] = 0.0f;
			m[1][3] = 0.0f;

			m[2][0] = 0.0f;
			m[2][1] = 0.0f;
			m[2][2] = 1.0f;
			m[2][3] = 0.0f;

			m[3][0] = 0.0f;
			m[3][1] = 0.0f;
			m[3][2] = 0.0f;
			m[3][3] = 1.0f;
		}
		ML_MATH_INLINE Mat4f::Mat4f(float val)
		{
			m[0][0] = val;
			m[0][1] = 0.0f;
			m[0][2] = 0.0f;
			m[0][3] = 0.0f;

			m[1][0] = 0.0f;
			m[1][1] = val;
			m[1][2] = 0.0f;
			m[1][3] = 0.0f;

			m[2][0] = 0.0f;
			m[2][1] = 0.0f;
			m[2][2] = val;
			m[2][3] = 0.0f;

			m[3][0] = 0.0f;
			m[3][1] = 0.0f;
			m[3][2] = 0.0f;
			m[3][3] = val;
		}
		ML_MATH_INLINE Mat4f::Mat4f(float** val)
		{
			m[0][0] = **val;
		}
		ML_MATH_INLINE Mat4f::Mat4f(float m00, float m01, float m02, float m03,
									float m10, float m11, float m12, float m13,
									float m20, float m21, float m22, float m23,
									float m30, float m31, float m32, float m33)
		{
			m[0][0] = m00;
			m[1][0] = m01;
			m[2][0] = m02;
			m[3][0] = m03;

			m[0][1] = m10;
			m[1][1] = m11;
			m[2][1] = m12;
			m[3][1] = m13;

			m[0][2] = m20;
			m[1][2] = m21;
			m[2][2] = m22;
			m[3][2] = m23;

			m[0][3] = m30;
			m[1][3] = m31;
			m[2][3] = m32;
			m[3][3] = m33;
		}
		//================================================================


		ML_MATH_INLINE Mat4f& Mat4f::operator =(const Mat4f &inVec)
		{
			this->m[0][0] = inVec.m[0][0];
			this->m[1][0] = inVec.m[1][0];
			this->m[2][0] = inVec.m[2][0];
			this->m[3][0] = inVec.m[3][0];

			this->m[0][1] = inVec.m[0][1];
			this->m[1][1] = inVec.m[1][1];
			this->m[2][1] = inVec.m[2][1];
			this->m[3][1] = inVec.m[3][1];

			this->m[0][2] = inVec.m[0][2];
			this->m[1][2] = inVec.m[1][2];
			this->m[2][2] = inVec.m[2][2];
			this->m[3][2] = inVec.m[3][2];

			this->m[0][3] = inVec.m[0][3];
			this->m[1][3] = inVec.m[1][3];
			this->m[2][3] = inVec.m[2][3];
			this->m[3][3] = inVec.m[3][3];
			return *this;
		}
		ML_MATH_INLINE Mat4f& Mat4f::operator =(const float &infloat)
		{
			this->m[0][0] = infloat;
			this->m[1][0] = 0.0f;
			this->m[2][0] = 0.0f;
			this->m[3][0] = 0.0f;

			this->m[0][1] = 0.0f;
			this->m[1][1] = infloat;
			this->m[2][1] = 0.0f;
			this->m[3][1] = 0.0f;

			this->m[0][2] = 0.0f;
			this->m[1][2] = 0.0f;
			this->m[2][2] = infloat;
			this->m[3][2] = 0.0f;

			this->m[0][3] = 0.0f;
			this->m[1][3] = 0.0f;
			this->m[2][3] = 0.0f;
			this->m[3][3] = infloat;
			return *this;
		}

		ML_MATH_INLINE Mat4f& Mat4f::operator+=(const Mat4f &inVec)
		{
			this->m[0][0] += inVec.m[0][0];
			this->m[1][0] += inVec.m[1][0];
			this->m[2][0] += inVec.m[2][0];
			this->m[3][0] += inVec.m[3][0];

			this->m[0][1] += inVec.m[0][1];
			this->m[1][1] += inVec.m[1][1];
			this->m[2][1] += inVec.m[2][1];
			this->m[3][1] += inVec.m[3][1];

			this->m[0][2] += inVec.m[0][2];
			this->m[1][2] += inVec.m[1][2];
			this->m[2][2] += inVec.m[2][2];
			this->m[3][2] += inVec.m[3][2];

			this->m[0][3] += inVec.m[0][3];
			this->m[1][3] += inVec.m[1][3];
			this->m[2][3] += inVec.m[2][3];
			this->m[3][3] += inVec.m[3][3];
			return *this;
		}

		ML_MATH_INLINE Mat4f& Mat4f::operator-=(const Mat4f &inVec)
		{
			this->m[0][0] -= inVec.m[0][0];
			this->m[1][0] -= inVec.m[1][0];
			this->m[2][0] -= inVec.m[2][0];
			this->m[3][0] -= inVec.m[3][0];

			this->m[0][1] -= inVec.m[0][1];
			this->m[1][1] -= inVec.m[1][1];
			this->m[2][1] -= inVec.m[2][1];
			this->m[3][1] -= inVec.m[3][1];

			this->m[0][2] -= inVec.m[0][2];
			this->m[1][2] -= inVec.m[1][2];
			this->m[2][2] -= inVec.m[2][2];
			this->m[3][2] -= inVec.m[3][2];

			this->m[0][3] -= inVec.m[0][3];
			this->m[1][3] -= inVec.m[1][3];
			this->m[2][3] -= inVec.m[2][3];
			this->m[3][3] -= inVec.m[3][3];
			return *this;
		}

		ML_MATH_INLINE Mat4f& Mat4f::operator*=(const Mat4f &inVec)
		{
			this->m[0][0] = (this->m[0][0] * inVec.m[0][0]) + (this->m[0][1] * inVec.m[1][0]) + (this->m[0][2] * inVec.m[2][0]) + (this->m[0][3] * inVec.m[3][0]);
			this->m[1][0] = (this->m[0][0] * inVec.m[0][1]) + (this->m[0][1] * inVec.m[1][1]) + (this->m[0][2] * inVec.m[2][1]) + (this->m[0][3] * inVec.m[3][1]);
			this->m[2][0] = (this->m[0][0] * inVec.m[0][2]) + (this->m[0][1] * inVec.m[1][2]) + (this->m[0][2] * inVec.m[2][2]) + (this->m[0][3] * inVec.m[3][2]);
			this->m[3][0] = (this->m[0][0] * inVec.m[0][3]) + (this->m[0][1] * inVec.m[1][3]) + (this->m[0][2] * inVec.m[2][3]) + (this->m[0][3] * inVec.m[3][3]);

			this->m[0][1] = (this->m[1][0] * inVec.m[0][0]) + (this->m[1][1] * inVec.m[1][0]) + (this->m[1][2] * inVec.m[2][0]) + (this->m[1][3] * inVec.m[3][0]);
			this->m[1][1] = (this->m[1][0] * inVec.m[0][1]) + (this->m[1][1] * inVec.m[1][1]) + (this->m[1][2] * inVec.m[2][1]) + (this->m[1][3] * inVec.m[3][1]);
			this->m[2][1] = (this->m[1][0] * inVec.m[0][2]) + (this->m[1][1] * inVec.m[1][2]) + (this->m[1][2] * inVec.m[2][2]) + (this->m[1][3] * inVec.m[3][2]);
			this->m[3][1] = (this->m[1][0] * inVec.m[0][3]) + (this->m[1][1] * inVec.m[1][3]) + (this->m[1][2] * inVec.m[2][3]) + (this->m[1][3] * inVec.m[3][3]);

			this->m[0][2] = (this->m[2][0] * inVec.m[0][0]) + (this->m[2][1] * inVec.m[1][0]) + (this->m[2][2] * inVec.m[2][0]) + (this->m[2][3] * inVec.m[3][0]);
			this->m[1][2] = (this->m[2][0] * inVec.m[0][1]) + (this->m[2][1] * inVec.m[1][1]) + (this->m[2][2] * inVec.m[2][1]) + (this->m[2][3] * inVec.m[3][1]);
			this->m[2][2] = (this->m[2][0] * inVec.m[0][2]) + (this->m[2][1] * inVec.m[1][2]) + (this->m[2][2] * inVec.m[2][2]) + (this->m[2][3] * inVec.m[3][2]);
			this->m[3][2] = (this->m[2][0] * inVec.m[0][3]) + (this->m[2][1] * inVec.m[1][3]) + (this->m[2][2] * inVec.m[2][3]) + (this->m[2][3] * inVec.m[3][3]);

			this->m[0][3] = (this->m[3][0] * inVec.m[0][0]) + (this->m[3][1] * inVec.m[1][0]) + (this->m[3][2] * inVec.m[2][0]) + (this->m[3][3] * inVec.m[3][0]);
			this->m[1][3] = (this->m[3][0] * inVec.m[0][1]) + (this->m[3][1] * inVec.m[1][1]) + (this->m[3][2] * inVec.m[2][1]) + (this->m[3][3] * inVec.m[3][1]);
			this->m[2][3] = (this->m[3][0] * inVec.m[0][2]) + (this->m[3][1] * inVec.m[1][2]) + (this->m[3][2] * inVec.m[2][2]) + (this->m[3][3] * inVec.m[3][2]);
			this->m[3][3] = (this->m[3][0] * inVec.m[0][3]) + (this->m[3][1] * inVec.m[1][3]) + (this->m[3][2] * inVec.m[2][3]) + (this->m[3][3] * inVec.m[3][3]);
			return *this;
		}

		ML_MATH_INLINE Mat4f& Mat4f::operator*=(const float &infloat)
		{
			this->m[0][0] *= infloat;
			this->m[1][0] *= infloat;
			this->m[2][0] *= infloat;
			this->m[3][0] *= infloat;

			this->m[0][1] *= infloat;
			this->m[1][1] *= infloat;
			this->m[2][1] *= infloat;
			this->m[3][1] *= infloat;

			this->m[0][2] *= infloat;
			this->m[1][2] *= infloat;
			this->m[2][2] *= infloat;
			this->m[3][2] *= infloat;

			this->m[0][3] *= infloat;
			this->m[1][3] *= infloat;
			this->m[2][3] *= infloat;
			this->m[3][3] *= infloat;
			return *this;
		}

		ML_MATH_INLINE Mat4f& Mat4f::operator/=(const float &infloat)
		{
			this->m[0][0] /= infloat;
			this->m[1][0] /= infloat;
			this->m[2][0] /= infloat;
			this->m[3][0] /= infloat;

			this->m[0][1] /= infloat;
			this->m[1][1] /= infloat;
			this->m[2][1] /= infloat;
			this->m[3][1] /= infloat;

			this->m[0][2] /= infloat;
			this->m[1][2] /= infloat;
			this->m[2][2] /= infloat;
			this->m[3][2] /= infloat;

			this->m[0][3] /= infloat;
			this->m[1][3] /= infloat;
			this->m[2][3] /= infloat;
			this->m[3][3] /= infloat;
			return *this;
		}

		ML_MATH_INLINE Mat4f Mat4f::operator+(const Mat4f &inVec)
		{
			return Mat4f(this->m[0][0] + inVec.m[0][0],
						 this->m[1][0] + inVec.m[1][0],
						 this->m[2][0] + inVec.m[2][0],
						 this->m[3][0] + inVec.m[3][0],

						 this->m[0][1] + inVec.m[0][1],
						 this->m[1][1] + inVec.m[1][1],
						 this->m[2][1] + inVec.m[2][1],
						 this->m[3][1] + inVec.m[3][1],

						 this->m[0][2] + inVec.m[0][2],
						 this->m[1][2] + inVec.m[1][2],
						 this->m[2][2] + inVec.m[2][2],
						 this->m[3][2] + inVec.m[3][2],

						 this->m[0][3] + inVec.m[0][3],
						 this->m[1][3] + inVec.m[1][3],
						 this->m[2][3] + inVec.m[2][3],
						 this->m[3][3] + inVec.m[3][3]);
		}

		ML_MATH_INLINE Mat4f Mat4f::operator-(const Mat4f &inVec)
		{
			return Mat4f(this->m[0][0] - inVec.m[0][0],
						 this->m[1][0] - inVec.m[1][0],
						 this->m[2][0] - inVec.m[2][0],
						 this->m[3][0] - inVec.m[3][0],

						 this->m[0][1] - inVec.m[0][1],
						 this->m[1][1] - inVec.m[1][1],
						 this->m[2][1] - inVec.m[2][1],
						 this->m[3][1] - inVec.m[3][1],

						 this->m[0][2] - inVec.m[0][2],
						 this->m[1][2] - inVec.m[1][2],
						 this->m[2][2] - inVec.m[2][2],
						 this->m[3][2] - inVec.m[3][2],

						 this->m[0][3] - inVec.m[0][3],
						 this->m[1][3] - inVec.m[1][3],
						 this->m[2][3] - inVec.m[2][3],
						 this->m[3][3] - inVec.m[3][3]);
		}

		ML_MATH_INLINE Mat4f Mat4f::operator*(const Mat4f &inVec)
		{
			return Mat4f((this->m[0][0] * inVec.m[0][0]) + (this->m[0][1] * inVec.m[1][0]) + (this->m[0][2] * inVec.m[2][0]) + (this->m[0][3] * inVec.m[3][0]),
						 (this->m[0][0] * inVec.m[0][1]) + (this->m[0][1] * inVec.m[1][1]) + (this->m[0][2] * inVec.m[2][1]) + (this->m[0][3] * inVec.m[3][1]),
						 (this->m[0][0] * inVec.m[0][2]) + (this->m[0][1] * inVec.m[1][2]) + (this->m[0][2] * inVec.m[2][2]) + (this->m[0][3] * inVec.m[3][2]),
						 (this->m[0][0] * inVec.m[0][3]) + (this->m[0][1] * inVec.m[1][3]) + (this->m[0][2] * inVec.m[2][3]) + (this->m[0][3] * inVec.m[3][3]),

						 (this->m[1][0] * inVec.m[0][0]) + (this->m[1][1] * inVec.m[1][0]) + (this->m[1][2] * inVec.m[2][0]) + (this->m[1][3] * inVec.m[3][0]),
						 (this->m[1][0] * inVec.m[0][1]) + (this->m[1][1] * inVec.m[1][1]) + (this->m[1][2] * inVec.m[2][1]) + (this->m[1][3] * inVec.m[3][1]),
						 (this->m[1][0] * inVec.m[0][2]) + (this->m[1][1] * inVec.m[1][2]) + (this->m[1][2] * inVec.m[2][2]) + (this->m[1][3] * inVec.m[3][2]),
						 (this->m[1][0] * inVec.m[0][3]) + (this->m[1][1] * inVec.m[1][3]) + (this->m[1][2] * inVec.m[2][3]) + (this->m[1][3] * inVec.m[3][3]),

						 (this->m[2][0] * inVec.m[0][0]) + (this->m[2][1] * inVec.m[1][0]) + (this->m[2][2] * inVec.m[2][0]) + (this->m[2][3] * inVec.m[3][0]),
						 (this->m[2][0] * inVec.m[0][1]) + (this->m[2][1] * inVec.m[1][1]) + (this->m[2][2] * inVec.m[2][1]) + (this->m[2][3] * inVec.m[3][1]),
						 (this->m[2][0] * inVec.m[0][2]) + (this->m[2][1] * inVec.m[1][2]) + (this->m[2][2] * inVec.m[2][2]) + (this->m[2][3] * inVec.m[3][2]),
						 (this->m[2][0] * inVec.m[0][3]) + (this->m[2][1] * inVec.m[1][3]) + (this->m[2][2] * inVec.m[2][3]) + (this->m[2][3] * inVec.m[3][3]),

						 (this->m[3][0] * inVec.m[0][0]) + (this->m[3][1] * inVec.m[1][0]) + (this->m[3][2] * inVec.m[2][0]) + (this->m[3][3] * inVec.m[3][0]),
						 (this->m[3][0] * inVec.m[0][1]) + (this->m[3][1] * inVec.m[1][1]) + (this->m[3][2] * inVec.m[2][1]) + (this->m[3][3] * inVec.m[3][1]),
						 (this->m[3][0] * inVec.m[0][2]) + (this->m[3][1] * inVec.m[1][2]) + (this->m[3][2] * inVec.m[2][2]) + (this->m[3][3] * inVec.m[3][2]),
						 (this->m[3][0] * inVec.m[0][3]) + (this->m[3][1] * inVec.m[1][3]) + (this->m[3][2] * inVec.m[2][3]) + (this->m[3][3] * inVec.m[3][3]));
		}

		ML_MATH_INLINE Mat4f Mat4f::operator*(const float &infloat)
		{
			return Mat4f(this->m[0][0] * infloat,
						 this->m[1][0] * infloat,
						 this->m[2][0] * infloat,
						 this->m[3][0] * infloat,

						 this->m[0][1] * infloat,
						 this->m[1][1] * infloat,
						 this->m[2][1] * infloat,
						 this->m[3][1] * infloat,

						 this->m[0][2] * infloat,
						 this->m[1][2] * infloat,
						 this->m[2][2] * infloat,
						 this->m[3][2] * infloat,

						 this->m[0][3] * infloat,
						 this->m[1][3] * infloat,
						 this->m[2][3] * infloat,
						 this->m[3][3] * infloat);
		}

		ML_MATH_INLINE Mat4f Mat4f::operator/(const float &infloat)
		{
			return Mat4f(this->m[0][0] / infloat,
						 this->m[1][0] / infloat,
						 this->m[2][0] / infloat,
						 this->m[3][0] / infloat,

						 this->m[0][1] / infloat,
						 this->m[1][1] / infloat,
						 this->m[2][1] / infloat,
						 this->m[3][1] / infloat,

						 this->m[0][2] / infloat,
						 this->m[1][2] / infloat,
						 this->m[2][2] / infloat,
						 this->m[3][2] / infloat,

						 this->m[0][3] / infloat,
						 this->m[1][3] / infloat,
						 this->m[2][3] / infloat,
						 this->m[3][3] / infloat);
		}
		//================================================================

		ML_MATH_INLINE float* toptr(const Mat4f &Mat4A)
		{
			return (float*)Mat4A.m;
		}


	}
}

#endif