#ifndef MLVECTOR3_H
#define MLVECTOR3_H
#include <math.h>
#include "MLMath/MLMathDefine.h"
#include "MLMath/MLStandardMath.h"

namespace MLMath
{
	namespace MLVector3
	{
		struct Vector3f
		{
			union
			{
				float v[3];
				struct  
				{
					float x;
					float y;
					float z;
				};
			};

			//constructors
			Vector3f();
			Vector3f(float inXYZ);
			Vector3f(float inX, float inY, float inZ);

			//= operator
			Vector3f& operator=(const Vector3f &inVec);
			Vector3f& operator=(const float &infloat);
			Vector3f& operator+=(const Vector3f &inVec);
			Vector3f& operator-=(const Vector3f &inVec);
			Vector3f& operator*=(const Vector3f &inVec);
			Vector3f& operator*=(const float &infloat);
			Vector3f& operator/=(const Vector3f &inVec);
			Vector3f operator+(const Vector3f &inVec);
			Vector3f operator-(const Vector3f &inVec);
			Vector3f operator*(const Vector3f &inVec);
			Vector3f operator*(const float &infloat);
			Vector3f operator/(const Vector3f &inVec);
			Vector3f operator/(const float &infloat);
		};


		float dot(const Vector3f &VecA, const Vector3f &VecB);
		float angle(const Vector3f &VecA, const Vector3f &VecB);
		float* toptr(const Vector3f& VecA);





		//constructors
		//================================================================
		ML_MATH_INLINE Vector3f::Vector3f() : x(v[0]), y(v[1]), z(v[2]) { v[0] = 0.0f; v[1] = 0.0f; v[2] = 0.0f; }
		ML_MATH_INLINE Vector3f::Vector3f(float inXYZ) : x(v[0]), y(v[1]), z(v[2]) {v[0] = inXYZ; v[1] = inXYZ; v[2] = inXYZ;}
		ML_MATH_INLINE Vector3f::Vector3f(float inX, float inY, float inZ) : x(v[0]), y(v[1]), z(v[2]) {v[0] = inX; v[1] = inY; v[2] = inZ;} 
		//================================================================

		//Basic Operators
		//================================================================
		ML_MATH_INLINE Vector3f& Vector3f::operator =(const Vector3f &inVec)
		{
			this->x = inVec.x;
			this->y = inVec.y;
			this->z = inVec.z;
			return *this;
		}
		ML_MATH_INLINE Vector3f& Vector3f::operator =(const float &infloat)
		{
			this->x = infloat;
			this->y = infloat;
			this->z = infloat;
			return *this;
		}

		ML_MATH_INLINE Vector3f& Vector3f::operator+=(const Vector3f &inVec)
		{
			this->x += inVec.x;
			this->y += inVec.y;
			this->z += inVec.z;
			return *this;
		}

		ML_MATH_INLINE Vector3f& Vector3f::operator-=(const Vector3f &inVec)
		{
			this->x -= inVec.x;
			this->y -= inVec.y;
			this->z -= inVec.z;
			return *this;
		}

		ML_MATH_INLINE Vector3f& Vector3f::operator*=(const Vector3f &inVec)
		{
			this->x *= inVec.x;
			this->y *= inVec.y;
			this->z *= inVec.z;
			return *this;
		}

		ML_MATH_INLINE Vector3f& Vector3f::operator*=(const float &infloat)
		{
			this->x *= infloat;
			this->y *= infloat;
			this->z *= infloat;
			return *this;
		}

		ML_MATH_INLINE Vector3f& Vector3f::operator/=(const Vector3f &inVec)
		{
			this->x /= inVec.x;
			this->y /= inVec.y;
			this->z /= inVec.z;
			return *this;
		}

		ML_MATH_INLINE Vector3f Vector3f::operator+(const Vector3f &inVec)
		{
			return Vector3f((this->x + inVec.x), (this->y + inVec.y), (this->z + inVec.z));
		}

		ML_MATH_INLINE Vector3f Vector3f::operator-(const Vector3f &inVec)
		{
			return Vector3f((this->x - inVec.x), (this->y - inVec.y), (this->z - inVec.z));
		}

		ML_MATH_INLINE Vector3f Vector3f::operator*(const Vector3f &inVec)
		{
			return Vector3f((this->x * inVec.x), (this->y * inVec.y), (this->z * inVec.z));
		}

		ML_MATH_INLINE Vector3f Vector3f::operator/(const Vector3f &inVec)
		{
			return Vector3f((this->x / inVec.x), (this->y / inVec.y), (this->z / inVec.z));
		}

		ML_MATH_INLINE Vector3f Vector3f::operator*(const float &infloat)
		{
			return Vector3f((this->x * infloat), (this->y * infloat),  (this->z * infloat));
		}

		ML_MATH_INLINE Vector3f Vector3f::operator/(const float &infloat)
		{
			return Vector3f((this->x / infloat), (this->y / infloat), (this->z / infloat));
		}

		//================================================================


		//vector3 math functions
		//================================================================

		ML_MATH_INLINE float dot(const Vector3f &VecA, const Vector3f &VecB)
		{
			return (VecA.x * VecB.x) + (VecA.y * VecB.y) + (VecA.z * VecB.z);
		}

		ML_MATH_INLINE float angle(const Vector3f &VecA, const Vector3f &VecB)
		{
			//return ML_Rad2Deg(acos(dot(VecA, VecB) / (sqrt(((VecA.x * VecA.x) + (VecA.y * VecA.y) + (VecA.z * VecA.z))) * sqrt(((VecB.x * VecB.x) + (VecB.y * VecB.y) + (VecB.z * VecB.z))))));
		}

		ML_MATH_INLINE float* toptr(const Vector3f &VecA)
		{
			return (float*)VecA.v;
		}

		//================================================================
	}
}

#endif