#ifndef MLVECTOR2_H
#define MLVECTOR2_H

#include <math.h>
#include "MLMath/MLMathDefine.h"
#include "MLMath/MLStandardMath.h"
namespace MLMath
{
	namespace MLVector2
	{
		struct Vector2f
		{
			union
			{
				float v[2];
				struct  
				{
					float x;
					float y;
				};
			};

			//constructors
			Vector2f();
			Vector2f(float inXY);
			Vector2f(float inX, float inY);

			//= operator
			Vector2f& operator=(const Vector2f &inVec);
			Vector2f& operator=(const float &infloat);
			Vector2f& operator+=(const Vector2f &inVec);
			Vector2f& operator-=(const Vector2f &inVec);
			Vector2f& operator*=(const Vector2f &inVec);
			Vector2f& operator*=(const float &infloat);
			Vector2f& operator/=(const Vector2f &inVec);
			Vector2f operator+(const Vector2f &inVec);
			Vector2f operator-(const Vector2f &inVec);
			Vector2f operator*(const Vector2f &inVec);
			Vector2f operator*(const float &infloat);
			Vector2f operator/(const Vector2f &inVec);
			Vector2f operator/(const float &infloat);
			//float* operator[](const int i);
		};

		float length(const Vector2f &VecA);
		Vector2f normalize(const Vector2f &VecA);
		float dot(const Vector2f &VecA, const Vector2f &VecB);
		//TO DO: FIX TO WORK 360 DEGREES
		float angle(const Vector2f &VecA, const Vector2f &VecB);

		float* toptr(const Vector2f &VecA);





		//constructors
		//================================================================
		ML_MATH_INLINE Vector2f::Vector2f() : x(v[0]), y(v[1]) { v[0] = 0.0f; v[1] = 0.0f;}
		ML_MATH_INLINE Vector2f::Vector2f(float inXY) : x(v[0]), y(v[1]) { v[0] = inXY; v[1] = inXY;}
		ML_MATH_INLINE Vector2f::Vector2f(float inX, float inY) : x(v[0]), y(v[1]) { v[0] = inX; v[1] = inY;} 
		//================================================================

		//Basic Operators
		//================================================================
		ML_MATH_INLINE Vector2f& Vector2f::operator =(const Vector2f &inVec)
		{
			this->v[0] = inVec.v[0];
			this->v[1] = inVec.v[1];
			return *this;
		}
		ML_MATH_INLINE Vector2f& Vector2f::operator =(const float &infloat)
		{
			this->v[0] = infloat;
			this->v[1] = infloat;
			return *this;
		}

		ML_MATH_INLINE Vector2f& Vector2f::operator+=(const Vector2f &inVec)
		{
			this->v[0] += inVec.v[0];
			this->v[1] += inVec.v[1];
			return *this;
		}

		ML_MATH_INLINE Vector2f& Vector2f::operator-=(const Vector2f &inVec)
		{
			this->v[0] -= inVec.v[0];
			this->v[1] -= inVec.v[1];
			return *this;
		}

		ML_MATH_INLINE Vector2f& Vector2f::operator*=(const Vector2f &inVec)
		{
			this->v[0] *= inVec.v[0];
			this->v[1] *= inVec.v[1];
			return *this;
		}

		ML_MATH_INLINE Vector2f& Vector2f::operator*=(const float &infloat)
		{
			this->v[0] *= infloat;
			this->v[1] *= infloat;
			return *this;
		}

		ML_MATH_INLINE Vector2f& Vector2f::operator/=(const Vector2f &inVec)
		{
			this->v[0] /= inVec.v[0];
			this->v[1] /= inVec.v[1];
			return *this;
		}

		ML_MATH_INLINE Vector2f Vector2f::operator+(const Vector2f &inVec)
		{
			return Vector2f((this->v[0] + inVec.v[0]), (this->v[1] + inVec.v[1]));
		}

		ML_MATH_INLINE Vector2f Vector2f::operator-(const Vector2f &inVec)
		{
			return Vector2f((this->v[0] - inVec.v[0]), (this->v[1] - inVec.v[1]));
		}

		ML_MATH_INLINE Vector2f Vector2f::operator*(const Vector2f &inVec)
		{
			return Vector2f((this->v[0] * inVec.v[0]), (this->v[1] * inVec.v[1]));
		}

		ML_MATH_INLINE Vector2f Vector2f::operator/(const Vector2f &inVec)
		{
			return Vector2f((this->v[0] / inVec.v[0]), (this->v[1] / inVec.v[1]));
		}

		ML_MATH_INLINE Vector2f Vector2f::operator*(const float &infloat)
		{
			return Vector2f((this->v[0] * infloat), (this->v[1] * infloat));
		}

		ML_MATH_INLINE Vector2f Vector2f::operator/(const float &infloat)
		{
			return Vector2f((this->v[0] / infloat), (this->v[1] / infloat));
		}

		// 		ML_MATH_INLINE float Vector2f::operator[](const int i)
		// 		{
		// 			//this could be wrong but worth a try
		// 			return (&x)[i];
		// 		}

		//================================================================

		//vector2 math functions
		//================================================================
		ML_MATH_INLINE float length(const Vector2f &VecA)
		{
			return sqrt((VecA.x * VecA.x) + (VecA.y * VecA.y));
		}

		ML_MATH_INLINE Vector2f normalize(const Vector2f &VecA)
		{
			float vLength = length(VecA);
			return Vector2f((VecA.x / vLength), (VecA.y / vLength));
		}

		ML_MATH_INLINE float dot(const Vector2f &VecA, const Vector2f &VecB)
		{
			return (VecA.x * VecB.x) + (VecA.y * VecB.y);
		}

		ML_MATH_INLINE float angle(const Vector2f &VecA, const Vector2f &VecB)
		{
			//float angleDeg =  ML_Rad2Deg(acos(dot(VecA, VecB) / (length(VecA) * length(VecB))));
			float angleDeg = ML_Rad2Deg((atan2(VecA.y, VecA.x) - atan2(VecB.y, VecB.x)));
			return  (angleDeg > 0.0f ? angleDeg : angleDeg + 360);
		}

		ML_MATH_INLINE float* toptr(const Vector2f &VecA)
		{
			return (float*)VecA.v;
		}
		//================================================================



	}
}

#endif