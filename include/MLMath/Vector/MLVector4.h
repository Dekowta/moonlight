#ifndef  MLVECTOR4_H
#define MLVECTOR4_H
#include <math.h>
#include "MLMath/MLMathDefine.h"
#include "MLMath/MLStandardMath.h"
namespace MLMath
{
	namespace MLVector4
	{
		struct Vector4f
		{
			union
			{
				float v[4];
				struct  
				{
					float x;
					float y;
					float z;
					float w;
				};
				struct
				{
					float x;
					float y;
					float Width;
					float Height;
				};
			};
			

			//constructors
			Vector4f();
			Vector4f(float inXYZW);
			Vector4f(float inX, float inY, float inZ, float inW);

			//= operator
			Vector4f& operator=(const Vector4f &inVec);
			Vector4f& operator=(const float &infloat);
			Vector4f& operator+=(const Vector4f &inVec);
			Vector4f& operator-=(const Vector4f &inVec);
			Vector4f& operator*=(const Vector4f &inVec);
			Vector4f& operator*=(const float &infloat);
			Vector4f& operator/=(const Vector4f &inVec);
			Vector4f operator+(const Vector4f &inVec);
			Vector4f operator-(const Vector4f &inVec);
			Vector4f operator*(const Vector4f &inVec);
			Vector4f operator*(const float &infloat);
			Vector4f operator/(const Vector4f &inVec);
			Vector4f operator/(const float &infloat);
			
		};


		float dot(const Vector4f &VecA, const Vector4f &VecB);
		float angle(const Vector4f &VecA, const Vector4f &VecB);
		float* toptr(const Vector4f& VecA);





		//constructors
		//================================================================
		ML_MATH_INLINE Vector4f::Vector4f() : x(v[0]), y(v[1]), z(v[2]), w(v[3]), Width(v[2]), Height(v[3]) { v[0] = 0.0f; v[1] = 0.0f; v[2] = 0.0f; }
		ML_MATH_INLINE Vector4f::Vector4f(float inXYZW) : x(v[0]), y(v[1]), z(v[2]), w(v[3]), Width(v[2]), Height(v[3]) {v[0] = inXYZW; v[1] = inXYZW; v[2] = inXYZW;}
		ML_MATH_INLINE Vector4f::Vector4f(float inX, float inY, float inZ,  float inW) : x(v[0]), y(v[1]), z(v[2]), w(v[3]), Width(v[2]), Height(v[3]) {v[0] = inX; v[1] = inY; v[2] = inZ; v[3] = inW;} 
		//================================================================

		//Basic Operators
		//================================================================
		ML_MATH_INLINE Vector4f& Vector4f::operator =(const Vector4f &inVec)
		{
			this->x = inVec.x;
			this->y = inVec.y;
			this->z = inVec.z;
			this->w = inVec.w;
			return *this;
		}
		ML_MATH_INLINE Vector4f& Vector4f::operator =(const float &infloat)
		{
			this->x = infloat;
			this->y = infloat;
			this->z = infloat;
			this->w = infloat;
			return *this;
		}

		ML_MATH_INLINE Vector4f& Vector4f::operator+=(const Vector4f &inVec)
		{
			this->x += inVec.x;
			this->y += inVec.y;
			this->z += inVec.z;
			this->w += inVec.w;
			return *this;
		}

		ML_MATH_INLINE Vector4f& Vector4f::operator-=(const Vector4f &inVec)
		{
			this->x -= inVec.x;
			this->y -= inVec.y;
			this->z -= inVec.z;
			this->w -= inVec.w;
			return *this;
		}

		ML_MATH_INLINE Vector4f& Vector4f::operator*=(const Vector4f &inVec)
		{
			this->x *= inVec.x;
			this->y *= inVec.y;
			this->z *= inVec.z;
			this->w *= inVec.w;
			return *this;
		}

		ML_MATH_INLINE Vector4f& Vector4f::operator*=(const float &infloat)
		{
			this->x *= infloat;
			this->y *= infloat;
			this->z *= infloat;
			this->w *= infloat;
			return *this;
		}

		ML_MATH_INLINE Vector4f& Vector4f::operator/=(const Vector4f &inVec)
		{
			this->x /= inVec.x;
			this->y /= inVec.y;
			this->z /= inVec.z;
			this->w /= inVec.w;
			return *this;
		}

		ML_MATH_INLINE Vector4f Vector4f::operator+(const Vector4f &inVec)
		{
			return Vector4f((this->x + inVec.x), (this->y + inVec.y), (this->z + inVec.z), (this->w + inVec.w));
		}

		ML_MATH_INLINE Vector4f Vector4f::operator-(const Vector4f &inVec)
		{
			return Vector4f((this->x - inVec.x), (this->y - inVec.y), (this->z - inVec.z), (this->w - inVec.w));
		}

		ML_MATH_INLINE Vector4f Vector4f::operator*(const Vector4f &inVec)
		{
			return Vector4f((this->x * inVec.x), (this->y * inVec.y), (this->z * inVec.z), (this->w * inVec.w));
		}

		ML_MATH_INLINE Vector4f Vector4f::operator/(const Vector4f &inVec)
		{
			return Vector4f((this->x / inVec.x), (this->y / inVec.y), (this->z / inVec.z), (this->w / inVec.w));
		}

		ML_MATH_INLINE Vector4f Vector4f::operator*(const float &infloat)
		{
			return Vector4f((this->x * infloat), (this->y * infloat),  (this->z * infloat), (this->w + infloat));
		}

		ML_MATH_INLINE Vector4f Vector4f::operator/(const float &infloat)
		{
			return Vector4f((this->x / infloat), (this->y / infloat), (this->z / infloat), (this->w / infloat));
		}

		//================================================================


		//vector3 math functions
		//================================================================

		ML_MATH_INLINE float dot(const Vector4f &VecA, const Vector4f &VecB)
		{
			return (VecA.x * VecB.x) + (VecA.y * VecB.y) + (VecA.z * VecB.z) + (VecA.w * VecB.w);
		}

		ML_MATH_INLINE float angle(const Vector4f &VecA, const Vector4f &VecB)
		{
			//return ML_Rad2Deg(acos(dot(VecA, VecB) / (sqrt(((VecA.x * VecA.x) + (VecA.y * VecA.y) + (VecA.z * VecA.z))) * sqrt(((VecB.x * VecB.x) + (VecB.y * VecB.y) + (VecB.z * VecB.z))))));
		}

		ML_MATH_INLINE float* toptr(const Vector4f &VecA)
		{
			return (float*)VecA.v;
		}

		//================================================================
	}
}


#endif // MLVECTOR4_H
